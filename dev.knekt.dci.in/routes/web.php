<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// namespace App\Http\Controllers;
// use CRUDBooster;
// $user = CRUDBooster::me();
// dd($user);
// App::setlocale('ar');

Route::get('/', function () {
   return redirect('admin/login');
});
Route::post('/category/update','AdminTblCategoryController@update');
Route::post('/company/update','AdminCompanyController@update');
Route::post('/user/update','AdminCmsUsersController@update');
Route::get('/user/delete/{id}','AdminCmsUsersController@user_delete');
Route::post('/get_company_phone','AdminIvrDetailsController@get_company_phone');

Route::post('ivr_upload_form','AdminIvrDetailsController@ivr_upload_form');
Route::post('admin/ivr_data','AdminIvrDetailsController@ivr_data');

Route::post('admin/ajax_add_category','AdminIvrDetailsController@ajax_add_category');
Route::post('admin/ajax_add_subcate','AdminIvrDetailsController@ajax_add_subcate');
Route::post('admin/ajax_update_data','AdminIvrDetailsController@ajax_update_data');
Route::post('admin/ajax_get_data','AdminIvrDetailsController@ajax_get_data');
Route::post('admin/ajax_clone','AdminIvrDetailsController@ajax_clone');
Route::post('admin/ajax_delete_data','AdminIvrDetailsController@ajax_delete_data');

Route::get('privacypolicy','AdminTblCmspageController@privacypolicy');

