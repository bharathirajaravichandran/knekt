-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: crud_ke
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cms_apicustom`
--

DROP TABLE IF EXISTS `cms_apicustom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_apicustom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_apicustom`
--

LOCK TABLES `cms_apicustom` WRITE;
/*!40000 ALTER TABLE `cms_apicustom` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_apicustom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_apikey`
--

DROP TABLE IF EXISTS `cms_apikey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_apikey` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_apikey`
--

LOCK TABLES `cms_apikey` WRITE;
/*!40000 ALTER TABLE `cms_apikey` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_apikey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_dashboard`
--

DROP TABLE IF EXISTS `cms_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_dashboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_dashboard`
--

LOCK TABLES `cms_dashboard` WRITE;
/*!40000 ALTER TABLE `cms_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_email_queues`
--

DROP TABLE IF EXISTS `cms_email_queues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_email_queues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_email_queues`
--

LOCK TABLES `cms_email_queues` WRITE;
/*!40000 ALTER TABLE `cms_email_queues` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_email_queues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_email_templates`
--

DROP TABLE IF EXISTS `cms_email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_email_templates`
--

LOCK TABLES `cms_email_templates` WRITE;
/*!40000 ALTER TABLE `cms_email_templates` DISABLE KEYS */;
INSERT INTO `cms_email_templates` VALUES (1,'Email Template Forgot Password Backend','forgot_password_backend',NULL,'<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>','[password]','System','system@crudbooster.com',NULL,'2018-03-06 04:11:13',NULL);
/*!40000 ALTER TABLE `cms_email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_logs`
--

DROP TABLE IF EXISTS `cms_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_logs`
--

LOCK TABLES `cms_logs` WRITE;
/*!40000 ALTER TABLE `cms_logs` DISABLE KEYS */;
INSERT INTO `cms_logs` VALUES (1,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-06 04:13:16',NULL),(2,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/users/add-save','Add New Data Bala at Users Management','',1,'2018-03-06 05:00:01',NULL),(3,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-06 05:00:19',NULL),(4,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','balamuruga.subiramanian@dci.in login with IP Address 127.0.0.1','',2,'2018-03-06 05:00:36',NULL),(5,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/tbl_category/add-save','Add New Data Amazon at Category','',2,'2018-03-06 05:15:01',NULL),(6,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/tbl_category/add-save','Add New Data Mens Clothes at Category','',2,'2018-03-06 05:16:25',NULL),(7,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/cat_service/add-save','Add New Data Customer Call at CategoryService','',2,'2018-03-06 05:39:52',NULL),(8,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/menu_management/edit-save/2','Update data Category Service at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>CategoryService</td><td>Category Service</td></tr><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>',2,'2018-03-06 06:41:11',NULL),(9,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/cat_service/add-save','Add New Data Customer skype at CategoryService','',2,'2018-03-06 07:24:24',NULL),(10,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/tbl_catoptions/add-save','Add New Data English at Catoption','',2,'2018-03-06 07:50:41',NULL),(11,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/tbl_catoptions/add-save','Add New Data Arabic at Catoption','',2,'2018-03-06 07:50:59',NULL),(12,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/tbl_catoptions/add-save','Add New Data Click for customer call at Catoption','',2,'2018-03-06 07:59:20',NULL),(13,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/tbl_catoptions/add-save','Add New Data skype for customer at Catoption','',2,'2018-03-06 08:00:15',NULL),(14,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/tbl_catoptions/edit-save/4','Update data skype for customer at Catoption','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',2,'2018-03-06 08:00:23',NULL),(15,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-08 07:07:30',NULL),(16,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-09 01:06:40',NULL),(17,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/menu_management/edit-save/3','Update data Category AVR at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Catoption</td><td>Category AVR</td></tr><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>',1,'2018-03-09 01:09:02',NULL),(18,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/users/add-save','Add New Data Hari at Users Management','',1,'2018-03-09 02:31:28',NULL),(19,'192.168.1.141','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 192.168.1.141','',3,'2018-03-09 02:37:47',NULL),(20,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/statistic_builder/add-save','Add New Data Category at Statistic Builder','',1,'2018-03-09 02:40:36',NULL),(21,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/menu_management/edit-save/2','Update data Category Service at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2018-03-09 02:42:15',NULL),(22,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/menu_management/edit-save/1','Update data Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>',1,'2018-03-09 02:42:21',NULL),(23,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/menu_management/edit-save/3','Update data Category AVR at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>',1,'2018-03-09 02:42:26',NULL),(24,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 127.0.0.1','',3,'2018-03-09 02:43:36',NULL),(25,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/menu_management/add-save','Add New Data Users at Menu Management','',1,'2018-03-09 02:46:04',NULL),(26,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/users/delete/2','Delete data Bala at Users Management','',3,'2018-03-09 02:46:39',NULL),(27,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/statistic_builder/add-save','Add New Data Graph at Statistic Builder','',1,'2018-03-09 02:49:34',NULL),(28,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/menu_management/edit-save/3','Update data Category IVR at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Category AVR</td><td>Category IVR</td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>',1,'2018-03-09 02:51:22',NULL),(29,'192.168.1.141','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36','http://kenkit.com/admin/logout','hari@gmail.com logout','',3,'2018-03-09 04:16:17',NULL),(30,'192.168.1.141','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 192.168.1.141','',3,'2018-03-09 04:27:32',NULL),(31,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 127.0.0.1','',3,'2018-03-12 07:47:02',NULL),(32,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/logout','hari@gmail.com logout','',3,'2018-03-12 07:47:12',NULL),(33,'192.168.1.141','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 192.168.1.141','',3,'2018-03-12 07:47:14',NULL),(34,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-12 07:47:23',NULL),(35,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/settings/delete/3','Delete data login_background_image at Settings','',1,'2018-03-12 07:51:00',NULL),(36,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-12 23:43:17',NULL),(37,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 127.0.0.1','',3,'2018-03-15 06:58:00',NULL),(38,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-15 22:47:49',NULL),(39,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-15 22:48:23',NULL),(40,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 127.0.0.1','',3,'2018-03-15 22:48:34',NULL),(41,'192.168.1.123','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36','http://www.kenkit.com/admin/login','hari@gmail.com login with IP Address 192.168.1.123','',3,'2018-03-15 23:33:05',NULL),(42,'192.168.1.123','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36','http://www.kenkit.com/admin/tbl_category/add-save','Add New Data school A at Category','',3,'2018-03-16 01:24:25',NULL),(43,'192.168.1.123','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36','http://www.kenkit.com/admin/tbl_category/add-save','Add New Data school B at Category','',3,'2018-03-16 01:25:36',NULL),(44,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-16 05:00:46',NULL),(45,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/tbl_category/action-selected','Delete data 4,3,2,1 at Category','',1,'2018-03-16 05:07:53',NULL),(46,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 05:08:47',NULL),(47,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-16 05:09:00',NULL),(48,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/tbl_category/add-save','Add New Data Auto Mobile at Category','',1,'2018-03-16 05:41:29',NULL),(49,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 06:18:19',NULL),(50,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 06:54:11',NULL),(51,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 06:54:20',NULL),(52,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 06:54:26',NULL),(53,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 06:54:32',NULL),(54,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 06:54:56',NULL),(55,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 06:55:01',NULL),(56,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 06:55:36',NULL),(57,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 06:55:56',NULL),(58,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 06:56:34',NULL),(59,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com تسجيل خروج','',1,'2018-03-16 06:59:05',NULL),(60,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-16 07:00:46',NULL),(61,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 07:01:00',NULL),(62,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 07:01:07',NULL),(63,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 07:01:34',NULL),(64,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 07:02:56',NULL),(65,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 07:06:35',NULL),(66,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 07:13:20',NULL),(67,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 07:13:24',NULL),(68,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 07:13:28',NULL),(69,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 07:18:20',NULL),(70,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com سجل دخل من رقم آيبي 127.0.0.1','',1,'2018-03-16 07:18:25',NULL),(71,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 07:33:47',NULL),(72,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-16 07:33:50',NULL),(73,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-16 07:38:57',NULL),(74,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-16 07:41:16',NULL),(75,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-18 22:46:20',NULL),(76,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/2','Update data Company Details at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Category Service</td><td>Company Details</td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>',1,'2018-03-18 22:54:29',NULL),(77,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/3','Update data IVR Details at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Category IVR</td><td>IVR Details</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>',1,'2018-03-18 22:54:51',NULL),(78,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/subcategory/add-save','Add New Data Honda Auto Mobile at Sub Category','',1,'2018-03-19 00:32:11',NULL),(79,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/subcategory/add-save','Add New Data General Motors Chevrolet at Sub Category','',1,'2018-03-19 00:35:14',NULL),(80,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/cat_service/action-selected','Delete data 2,1 at CategoryService','',1,'2018-03-19 00:53:08',NULL),(81,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/module_generator/delete/13','Delete data CategoryService at Module Generator','',1,'2018-03-19 00:53:40',NULL),(82,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/6','Update data Company Details at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>company</td><td>Company Details</td></tr><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>',1,'2018-03-19 02:10:14',NULL),(83,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/tbl_catoptions/action-selected','Delete data 4,3,2,1 at Catoption','',1,'2018-03-19 03:55:17',NULL),(84,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/add-save','Add New Data BMW at company','',1,'2018-03-19 04:40:30',NULL),(85,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/tbl_category/edit-save/1','Update data Auto Mobile1 at Category','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>category_name</td><td>Auto Mobile</td><td>Auto Mobile1</td></tr><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 04:46:14',NULL),(86,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/add-save','Add New Data BMW1 at company','',1,'2018-03-19 04:55:30',NULL),(87,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/action-selected','Delete data 1 at company','',1,'2018-03-19 04:55:37',NULL),(88,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/action-selected','Delete data 2 at company','',1,'2018-03-19 05:00:10',NULL),(89,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/add-save','Add New Data BMW at company','',1,'2018-03-19 05:04:19',NULL),(90,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/edit-save/1','Update data BMW1 at company','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 05:04:35',NULL),(91,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/edit-save/1','Update data BMW1 at company','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>phone</td><td>12343432432</td><td>343432432</td></tr><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 05:09:58',NULL),(92,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/edit-save/1','Update data BMW1 at company','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>phone</td><td>343432432</td><td>3434</td></tr><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 05:12:51',NULL),(93,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/edit-save/1','Update data BMW1 at company','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 05:14:17',NULL),(94,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/edit-save/1','Update data BMW1 at company','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 05:14:22',NULL),(95,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/edit-save/1','Update data BMW1 at company','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 05:14:42',NULL),(96,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/edit-save/1','Update data BMW1 at company','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>phone</td><td>3434</td><td>34341</td></tr><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 05:14:49',NULL),(97,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/add-save','Add New Data Audi at company','',1,'2018-03-19 05:38:06',NULL),(98,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/company/edit-save/2','Update data Audi at company','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>phone</td><td>+852234324324</td><td>234324324</td></tr><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-19 05:38:23',NULL),(99,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/location/add-save','Add New Data 2 at location','',1,'2018-03-19 07:22:55',NULL),(100,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/location/edit-save/1','Update data 2 at location','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>latitude</td><td>10</td><td>11.7467759</td></tr><tr><td>langitude</td><td>78</td><td>79.73647200000005</td></tr><tr><td>address</td><td>Madurai Madurai TN</td><td>MADURAI VEERAN NAGAR SH 68 Koothapakkam</td></tr></tbody></table>',1,'2018-03-19 07:23:41',NULL),(101,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/location/add-save','Add New Data 1 at location','',1,'2018-03-19 07:30:52',NULL),(102,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/location/add-save','Add New Data 2 at location','',1,'2018-03-19 07:31:42',NULL),(103,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/module_generator/delete/14','Delete data Catoption at Module Generator','',1,'2018-03-19 08:06:14',NULL),(104,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/location/add-save','Add New Data 2 at location','',1,'2018-03-19 08:17:05',NULL),(105,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-19 23:09:42',NULL),(106,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/7','Update data Company location at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>location</td><td>Company location</td></tr><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-map-marker</td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>',1,'2018-03-19 23:12:54',NULL),(107,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/6','Update data Company Details at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-list-alt</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>',1,'2018-03-19 23:13:29',NULL),(108,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/1','Update data Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-certificate</td><td>fa fa-cube</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>',1,'2018-03-19 23:15:33',NULL),(109,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/5','Update data Sub Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>light-blue</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-cubes</td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>',1,'2018-03-19 23:15:55',NULL),(110,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/1','Update data Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>red</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>',1,'2018-03-19 23:16:07',NULL),(111,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/1','Update data Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>is_dashboard</td><td>0</td><td>1</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>',1,'2018-03-19 23:16:22',NULL),(112,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/1','Update data Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>',1,'2018-03-19 23:16:38',NULL),(113,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/6','Update data Company Details at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>green</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>',1,'2018-03-19 23:16:57',NULL),(114,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/7','Update data Company location at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>aqua</td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>',1,'2018-03-19 23:17:10',NULL),(115,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/statistic_builder/action-selected','Delete data 2,1 at Statistic Builder','',1,'2018-03-19 23:29:45',NULL),(116,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/statistic_builder/add-save','Add New Data Mail Dashboard at Statistic Builder','',1,'2018-03-19 23:33:29',NULL),(117,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/statistic_builder/edit-save/1','Update data Main Dashboard at Statistic Builder','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Mail Dashboard</td><td>Main Dashboard</td></tr><tr><td>slug</td><td>mail-dashboard</td><td></td></tr></tbody></table>',1,'2018-03-19 23:33:39',NULL),(118,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/add-save','Add New Data Statistics at Menu Management','',1,'2018-03-20 00:05:03',NULL),(119,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/8','Update data Statistics at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>admin/statistic_builder/builder</td><td></td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>',1,'2018-03-20 00:09:45',NULL),(120,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/add-save','Add New Data dashboard at Menu Management','',1,'2018-03-20 00:10:25',NULL),(121,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/8','Update data Statistics at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>URL</td><td>Statistic</td></tr><tr><td>path</td><td>admin/statistic_builder/builder</td><td>statistic_builder/show/mail-dashboard</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>',1,'2018-03-20 00:11:35',NULL),(122,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/delete/9','Delete data dashboard at Menu Management','',1,'2018-03-20 00:11:39',NULL),(123,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Arabic at IVR Details','',1,'2018-03-20 02:09:53',NULL),(124,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data English at IVR Details','',1,'2018-03-20 02:10:09',NULL),(125,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/edit-save/2','Update data English at IVR Details','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-20 02:17:53',NULL),(126,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Service and spare parts at IVR Details','',1,'2018-03-20 02:18:10',NULL),(127,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Automotive at IVR Details','',1,'2018-03-20 02:18:28',NULL),(128,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Motorcycle at IVR Details','',1,'2018-03-20 02:18:53',NULL),(129,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Power products at IVR Details','',1,'2018-03-20 02:19:11',NULL),(130,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Customer service rep at IVR Details','',1,'2018-03-20 02:19:30',NULL),(131,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Book a service at IVR Details','',1,'2018-03-20 02:49:27',NULL),(132,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Spare parts at IVR Details','',1,'2018-03-20 02:49:41',NULL),(133,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Safat alghanim showroom at IVR Details','',1,'2018-03-20 02:50:54',NULL),(134,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data shuwaikh showroom at IVR Details','',1,'2018-03-20 02:51:49',NULL),(135,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data service at IVR Details','',1,'2018-03-20 02:52:01',NULL),(136,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data customer service at IVR Details','',1,'2018-03-20 02:52:14',NULL),(137,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data repeat same menu at IVR Details','',1,'2018-03-20 02:52:35',NULL),(138,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/module_generator/delete/19','Delete data IVR Sub Details at Module Generator','',1,'2018-03-20 02:53:59',NULL),(139,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-20 07:10:30',NULL),(140,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-21 00:19:52',NULL),(141,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data English at IVR Details','',1,'2018-03-21 00:23:14',NULL),(142,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Test to Service at IVR Details','',1,'2018-03-21 00:31:47',NULL),(143,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Test at IVR Details','',1,'2018-03-21 00:43:16',NULL),(144,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/action-selected','Delete data 17,16 at IVR Details','',1,'2018-03-21 00:43:29',NULL),(145,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Test2 at IVR Details','',1,'2018-03-21 00:43:40',NULL),(146,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/action-selected','Delete data 16 at IVR Details','',1,'2018-03-21 00:44:51',NULL),(147,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','admin@crudbooster.com logout','',1,'2018-03-21 00:48:42',NULL),(148,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-03-21 00:48:46',NULL),(149,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Arbic at IVR Details','',1,'2018-03-21 00:54:56',NULL),(150,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Test at IVR Details','',1,'2018-03-21 00:58:06',NULL),(151,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Test4 at IVR Details','',1,'2018-03-21 01:07:12',NULL),(152,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/delete/17','Delete data Test at IVR Details','',1,'2018-03-21 01:07:18',NULL),(153,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/ivr_details/add-save','Add New Data Test at IVR Details','',1,'2018-03-21 01:07:39',NULL),(154,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/1','Update data Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>red</td><td>normal</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>',1,'2018-03-21 02:13:02',NULL),(155,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/5','Update data Sub Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>light-blue</td><td>normal</td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>',1,'2018-03-21 02:13:14',NULL),(156,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/6','Update data Company Details at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>green</td><td>normal</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>',1,'2018-03-21 02:13:23',NULL),(157,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/7','Update data Company location at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>aqua</td><td>normal</td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>',1,'2018-03-21 02:13:30',NULL),(158,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/users/edit-save/1','Update data Developer at Users Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Super Admin</td><td>Developer</td></tr><tr><td>photo</td><td></td><td>uploads/1/2018-03/dci_logo.png</td></tr><tr><td>email</td><td>admin@crudbooster.com</td><td>developer@gmail.com</td></tr><tr><td>password</td><td>$2y$10$cIdzUT9vyn1lNnJQrJ3DmOz6ilOd55nlelWeRQbaPoOO5g.S1OXmu</td><td></td></tr><tr><td>phone</td><td></td><td>2132343</td></tr><tr><td>language</td><td>en</td><td></td></tr></tbody></table>',1,'2018-03-21 02:25:27',NULL),(159,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','developer@gmail.com logout','',1,'2018-03-21 02:25:45',NULL),(160,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','developer@gmail.com login with IP Address 127.0.0.1','',1,'2018-03-21 02:25:58',NULL),(161,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','developer@gmail.com logout','',1,'2018-03-21 03:21:16',NULL),(162,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 127.0.0.1','',3,'2018-03-21 03:21:22',NULL),(163,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/logout','hari@gmail.com logout','',3,'2018-03-21 03:21:50',NULL),(164,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/login','developer@gmail.com login with IP Address 127.0.0.1','',1,'2018-03-21 03:22:01',NULL),(165,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.167 Chrome/64.0.3282.167 Safari/537.36','http://kenkit.com/admin/login','hari@gmail.com login with IP Address 127.0.0.1','',3,'2018-03-21 03:23:25',NULL),(166,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/5','Update data Sub Category at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>',1,'2018-03-21 03:24:20',NULL),(167,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/6','Update data Company Details at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>',1,'2018-03-21 03:24:24',NULL),(168,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/7','Update data Company location at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>',1,'2018-03-21 03:24:31',NULL),(169,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0','http://kenkit.com/admin/menu_management/edit-save/10','Update data IVR Details at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>7</td><td></td></tr></tbody></table>',1,'2018-03-21 03:24:38',NULL);
/*!40000 ALTER TABLE `cms_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menus`
--

DROP TABLE IF EXISTS `cms_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menus`
--

LOCK TABLES `cms_menus` WRITE;
/*!40000 ALTER TABLE `cms_menus` DISABLE KEYS */;
INSERT INTO `cms_menus` VALUES (1,'Category','Route','AdminTblCategoryControllerGetIndex','normal','fa fa-cube',0,1,0,1,2,'2018-03-06 05:04:18','2018-03-21 02:13:01'),(4,'Users','Module','users','normal','fa fa-users',0,1,0,1,1,'2018-03-09 02:46:04',NULL),(5,'Sub Category','Route','AdminSubcategoryControllerGetIndex','normal','fa fa-cubes',0,1,0,1,3,'2018-03-18 22:55:30','2018-03-21 03:24:19'),(6,'Company Details','Route','AdminCompanyControllerGetIndex','normal','fa fa-list-alt',0,1,0,1,4,'2018-03-19 02:04:18','2018-03-21 03:24:24'),(7,'Company location','Route','AdminLocationControllerGetIndex','normal','fa fa-map-marker',0,1,0,1,6,'2018-03-19 07:10:07','2018-03-21 03:24:30'),(8,'Statistics','Statistic','statistic_builder/show/mail-dashboard','normal','fa fa-dashboard',0,1,1,1,NULL,'2018-03-20 00:05:02','2018-03-20 00:11:35'),(10,'IVR Details','Route','AdminIvrDetailsControllerGetIndex','normal','fa fa-glass',0,1,0,1,7,'2018-03-20 02:03:34','2018-03-21 03:24:38');
/*!40000 ALTER TABLE `cms_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menus_privileges`
--

DROP TABLE IF EXISTS `cms_menus_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menus_privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menus_privileges`
--

LOCK TABLES `cms_menus_privileges` WRITE;
/*!40000 ALTER TABLE `cms_menus_privileges` DISABLE KEYS */;
INSERT INTO `cms_menus_privileges` VALUES (10,4,2),(13,2,2),(14,2,1),(15,3,2),(16,3,1),(33,9,2),(34,9,1),(35,8,2),(36,8,1),(38,11,1),(39,1,2),(40,1,1),(45,5,2),(46,5,1),(47,6,2),(48,6,1),(49,7,2),(50,7,1),(51,10,2),(52,10,1);
/*!40000 ALTER TABLE `cms_menus_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_moduls`
--

DROP TABLE IF EXISTS `cms_moduls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_moduls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_moduls`
--

LOCK TABLES `cms_moduls` WRITE;
/*!40000 ALTER TABLE `cms_moduls` DISABLE KEYS */;
INSERT INTO `cms_moduls` VALUES (1,'Notifications','fa fa-cog','notifications','cms_notifications','NotificationsController',1,1,'2018-03-06 04:11:11',NULL,NULL),(2,'Privileges','fa fa-cog','privileges','cms_privileges','PrivilegesController',1,1,'2018-03-06 04:11:11',NULL,NULL),(3,'Privileges Roles','fa fa-cog','privileges_roles','cms_privileges_roles','PrivilegesRolesController',1,1,'2018-03-06 04:11:11',NULL,NULL),(4,'Users Management','fa fa-users','users','cms_users','AdminCmsUsersController',0,1,'2018-03-06 04:11:11',NULL,NULL),(5,'Settings','fa fa-cog','settings','cms_settings','SettingsController',1,1,'2018-03-06 04:11:11',NULL,NULL),(6,'Module Generator','fa fa-database','module_generator','cms_moduls','ModulsController',1,1,'2018-03-06 04:11:11',NULL,NULL),(7,'Menu Management','fa fa-bars','menu_management','cms_menus','MenusController',1,1,'2018-03-06 04:11:11',NULL,NULL),(8,'Email Templates','fa fa-envelope-o','email_templates','cms_email_templates','EmailTemplatesController',1,1,'2018-03-06 04:11:11',NULL,NULL),(9,'Statistic Builder','fa fa-dashboard','statistic_builder','cms_statistics','StatisticBuilderController',1,1,'2018-03-06 04:11:11',NULL,NULL),(10,'API Generator','fa fa-cloud-download','api_generator','','ApiCustomController',1,1,'2018-03-06 04:11:11',NULL,NULL),(11,'Log User Access','fa fa-flag-o','logs','cms_logs','LogsController',1,1,'2018-03-06 04:11:11',NULL,NULL),(12,'Category','fa fa-certificate','tbl_category','tbl_category','AdminTblCategoryController',0,0,'2018-03-06 05:04:17',NULL,NULL),(13,'CategoryService','fa fa-envelope-o','cat_service','tbl_service','AdminCatServiceController',0,0,'2018-03-06 05:33:12',NULL,'2018-03-19 00:53:40'),(14,'Catoption','fa fa-heart','tbl_catoptions','tbl_catoptions','AdminTblCatoptionsController',0,0,'2018-03-06 07:38:46',NULL,'2018-03-19 08:06:14'),(15,'Sub Category','fa fa-glass','subcategory','tbl_category','AdminSubcategoryController',0,0,'2018-03-18 22:55:30',NULL,NULL),(16,'company','fa fa-glass','company','tbl_service','AdminCompanyController',0,0,'2018-03-19 02:04:17',NULL,NULL),(17,'location','fa fa-glass','location','tbl_location','AdminLocationController',0,0,'2018-03-19 07:10:06',NULL,NULL),(18,'IVR Details','fa fa-glass','ivr_details','tbl_catoptions','AdminIvrDetailsController',0,0,'2018-03-20 02:03:33',NULL,NULL),(19,'IVR Sub Details','fa fa-glass','ivr_subdetails','tbl_catoptions','AdminIvrSubdetailsController',0,0,'2018-03-20 02:06:40',NULL,'2018-03-20 02:53:59');
/*!40000 ALTER TABLE `cms_moduls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_notifications`
--

DROP TABLE IF EXISTS `cms_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_notifications`
--

LOCK TABLES `cms_notifications` WRITE;
/*!40000 ALTER TABLE `cms_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_privileges`
--

DROP TABLE IF EXISTS `cms_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_privileges`
--

LOCK TABLES `cms_privileges` WRITE;
/*!40000 ALTER TABLE `cms_privileges` DISABLE KEYS */;
INSERT INTO `cms_privileges` VALUES (1,'Super Administrator',1,'skin-red','2018-03-06 04:11:11',NULL),(2,'Admin',0,'skin-red',NULL,NULL);
/*!40000 ALTER TABLE `cms_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_privileges_roles`
--

DROP TABLE IF EXISTS `cms_privileges_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_privileges_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_privileges_roles`
--

LOCK TABLES `cms_privileges_roles` WRITE;
/*!40000 ALTER TABLE `cms_privileges_roles` DISABLE KEYS */;
INSERT INTO `cms_privileges_roles` VALUES (1,1,0,0,0,0,1,1,'2018-03-06 04:11:11',NULL),(2,1,1,1,1,1,1,2,'2018-03-06 04:11:11',NULL),(3,0,1,1,1,1,1,3,'2018-03-06 04:11:11',NULL),(4,1,1,1,1,1,1,4,'2018-03-06 04:11:12',NULL),(5,1,1,1,1,1,1,5,'2018-03-06 04:11:12',NULL),(6,1,1,1,1,1,1,6,'2018-03-06 04:11:12',NULL),(7,1,1,1,1,1,1,7,'2018-03-06 04:11:12',NULL),(8,1,1,1,1,1,1,8,'2018-03-06 04:11:12',NULL),(9,1,1,1,1,1,1,9,'2018-03-06 04:11:12',NULL),(10,1,1,1,1,1,1,10,'2018-03-06 04:11:12',NULL),(11,1,0,1,0,1,1,11,'2018-03-06 04:11:12',NULL),(12,1,1,1,1,1,1,12,NULL,NULL),(13,1,1,1,1,1,1,13,NULL,NULL),(14,1,1,1,1,1,1,14,NULL,NULL),(15,1,1,1,1,1,2,12,NULL,NULL),(16,1,1,1,1,1,2,13,NULL,NULL),(17,1,1,1,1,1,2,14,NULL,NULL),(18,1,1,1,0,0,2,4,NULL,NULL),(19,1,1,1,1,1,1,15,NULL,NULL),(20,1,1,1,1,1,1,16,NULL,NULL),(21,1,1,1,1,1,1,17,NULL,NULL),(22,1,1,1,1,1,1,18,NULL,NULL),(23,1,1,1,1,1,1,19,NULL,NULL),(24,1,1,1,1,1,2,16,NULL,NULL),(25,1,1,1,1,1,2,18,NULL,NULL),(26,1,1,1,1,1,2,17,NULL,NULL),(27,1,1,1,1,1,2,15,NULL,NULL);
/*!40000 ALTER TABLE `cms_privileges_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_settings`
--

DROP TABLE IF EXISTS `cms_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_settings`
--

LOCK TABLES `cms_settings` WRITE;
/*!40000 ALTER TABLE `cms_settings` DISABLE KEYS */;
INSERT INTO `cms_settings` VALUES (1,'login_background_color',NULL,'text',NULL,'Input hexacode','2018-03-06 04:11:12',NULL,'Login Register Style','Login Background Color'),(2,'login_font_color',NULL,'text',NULL,'Input hexacode','2018-03-06 04:11:12',NULL,'Login Register Style','Login Font Color'),(4,'email_sender','support@crudbooster.com','text',NULL,NULL,'2018-03-06 04:11:12',NULL,'Email Setting','Email Sender'),(5,'smtp_driver','mail','select','smtp,mail,sendmail',NULL,'2018-03-06 04:11:12',NULL,'Email Setting','Mail Driver'),(6,'smtp_host','','text',NULL,NULL,'2018-03-06 04:11:12',NULL,'Email Setting','SMTP Host'),(7,'smtp_port','25','text',NULL,'default 25','2018-03-06 04:11:12',NULL,'Email Setting','SMTP Port'),(8,'smtp_username','','text',NULL,NULL,'2018-03-06 04:11:12',NULL,'Email Setting','SMTP Username'),(9,'smtp_password','','text',NULL,NULL,'2018-03-06 04:11:12',NULL,'Email Setting','SMTP Password'),(10,'appname','Knekt2','text',NULL,NULL,'2018-03-06 04:11:12',NULL,'Application Setting','Application Name'),(11,'default_paper_size','Legal','text',NULL,'Paper size, ex : A4, Legal, etc','2018-03-06 04:11:12',NULL,'Application Setting','Default Paper Print Size'),(12,'logo','uploads/2018-03/6e02c0c4a9016c320236af24d2cecf16.png','upload_image',NULL,NULL,'2018-03-06 04:11:12',NULL,'Application Setting','Logo'),(13,'favicon',NULL,'upload_image',NULL,NULL,'2018-03-06 04:11:12',NULL,'Application Setting','Favicon'),(14,'api_debug_mode','true','select','true,false',NULL,'2018-03-06 04:11:12',NULL,'Application Setting','API Debug Mode'),(15,'google_api_key','AIzaSyBdkLaZtAdO8mLytloT8OUZbYwLK-cOSxM','text',NULL,NULL,'2018-03-06 04:11:12',NULL,'Application Setting','Google API Key'),(16,'google_fcm_key',NULL,'text',NULL,NULL,'2018-03-06 04:11:12',NULL,'Application Setting','Google FCM Key');
/*!40000 ALTER TABLE `cms_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_statistic_components`
--

DROP TABLE IF EXISTS `cms_statistic_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_statistic_components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_statistic_components`
--

LOCK TABLES `cms_statistic_components` WRITE;
/*!40000 ALTER TABLE `cms_statistic_components` DISABLE KEYS */;
INSERT INTO `cms_statistic_components` VALUES (1,1,'6a30ce72a199906019e420241e46cac1','smallbox','area2',1,NULL,'{\"name\":\"Category\",\"icon\":\"ion-female\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/kenkit.com\\/admin\\/tbl_category\",\"sql\":\"select count(id) as name from tbl_category where parent_category_id=0 and status=1\"}','2018-03-09 02:50:02',NULL),(2,1,'2583690e9c0b6b9d1c95c757c26dcbc7','smallbox','area3',0,NULL,'{\"name\":\"Sub Category\",\"icon\":\"ion-transgender\",\"color\":\"bg-red\",\"link\":\"http:\\/\\/kenkit.com\\/admin\\/subcategory\",\"sql\":\"select count(id) as name from tbl_category where parent_category_id !=0 and status=1\"}','2018-03-19 23:41:24',NULL),(3,1,'3b2085a3f24fc654bca8c325630988b4','smallbox','area4',0,NULL,'{\"name\":\"Company\",\"icon\":\"ion-cube\",\"color\":\"bg-yellow\",\"link\":\"http:\\/\\/kenkit.com\\/admin\\/company\",\"sql\":\"select count(id) as name from tbl_service where status=1\"}','2018-03-19 23:43:07',NULL),(4,1,'3b163a77e87c2ff0b3b76f534c28d67a','smallbox',NULL,1,'Untitled',NULL,'2018-03-19 23:58:32',NULL),(5,1,'d07a4acec26600e5380647e5c2e55fc5','smallbox',NULL,0,'Untitled',NULL,'2018-03-19 23:58:39',NULL),(6,1,'3dd9600313cd1f6715dd18c15b99ab87','smallbox','area1',0,NULL,'{\"name\":\"Users\",\"icon\":\"ion-person\",\"color\":\"bg-aqua\",\"link\":\"http:\\/\\/kenkit.com\\/admin\\/users\",\"sql\":\"select count(id) as name from cms_users where status=1\"}','2018-03-19 23:58:40',NULL),(7,1,'513cb911f33e8d3cebbb8b223944b99d','chartarea',NULL,0,'Untitled',NULL,'2018-03-20 00:57:58',NULL),(8,1,'e85bfc66c8314c3adcd4c47dd97154a9','chartarea',NULL,0,'Untitled',NULL,'2018-03-20 00:58:03',NULL);
/*!40000 ALTER TABLE `cms_statistic_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_statistics`
--

DROP TABLE IF EXISTS `cms_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_statistics`
--

LOCK TABLES `cms_statistics` WRITE;
/*!40000 ALTER TABLE `cms_statistics` DISABLE KEYS */;
INSERT INTO `cms_statistics` VALUES (1,'Main Dashboard','mail-dashboard','2018-03-19 23:33:29','2018-03-19 23:33:39');
/*!40000 ALTER TABLE `cms_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_users`
--

DROP TABLE IF EXISTS `cms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0-inactive,1-active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_users`
--

LOCK TABLES `cms_users` WRITE;
/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
INSERT INTO `cms_users` VALUES (1,'Developer','uploads/1/2018-03/dci_logo.png','developer@gmail.com','$2y$10$cIdzUT9vyn1lNnJQrJ3DmOz6ilOd55nlelWeRQbaPoOO5g.S1OXmu','2132343','en',1,'2018-03-06 04:11:11','2018-03-21 02:25:27',1),(3,'Hari','uploads/1/2018-03/amazon.png','hari@gmail.com','$2y$10$.mqp5YFXwNSlsQE./K.Pp.3W1Gmxd0KM4GXEtjtP6QgkwlE6cwiU.','463746378',NULL,2,'2018-03-09 02:31:28',NULL,1);
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2016_08_07_145904_add_table_cms_apicustom',1),(2,'2016_08_07_150834_add_table_cms_dashboard',1),(3,'2016_08_07_151210_add_table_cms_logs',1),(4,'2016_08_07_151211_add_details_cms_logs',1),(5,'2016_08_07_152014_add_table_cms_privileges',1),(6,'2016_08_07_152214_add_table_cms_privileges_roles',1),(7,'2016_08_07_152320_add_table_cms_settings',1),(8,'2016_08_07_152421_add_table_cms_users',1),(9,'2016_08_07_154624_add_table_cms_menus_privileges',1),(10,'2016_08_07_154624_add_table_cms_moduls',1),(11,'2016_08_17_225409_add_status_cms_users',1),(12,'2016_08_20_125418_add_table_cms_notifications',1),(13,'2016_09_04_033706_add_table_cms_email_queues',1),(14,'2016_09_16_035347_add_group_setting',1),(15,'2016_09_16_045425_add_label_setting',1),(16,'2016_09_17_104728_create_nullable_cms_apicustom',1),(17,'2016_10_01_141740_add_method_type_apicustom',1),(18,'2016_10_01_141846_add_parameters_apicustom',1),(19,'2016_10_01_141934_add_responses_apicustom',1),(20,'2016_10_01_144826_add_table_apikey',1),(21,'2016_11_14_141657_create_cms_menus',1),(22,'2016_11_15_132350_create_cms_email_templates',1),(23,'2016_11_15_190410_create_cms_statistics',1),(24,'2016_11_17_102740_create_cms_statistic_components',1),(25,'2017_06_06_164501_add_deleted_at_cms_moduls',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `category_description` varchar(255) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `parent_category_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(100) NOT NULL DEFAULT 'en',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_category`
--

LOCK TABLES `tbl_category` WRITE;
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` VALUES (1,'Auto Mobile1','Auto mmobike','uploads/1/2018-03/clothe.jpg',0,'en','2018-03-16 05:41:29','2018-03-19 04:46:14',1),(2,'Honda Auto Mobile','Auto spare parts','uploads/1/2018-03/auto.jpeg',1,'en','2018-03-19 00:32:11',NULL,1),(3,'General Motors Chevrolet','General Motors Chevrolet','uploads/1/2018-03/index.jpeg',1,'en','2018-03-19 00:35:14',NULL,1);
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_catoptions`
--

DROP TABLE IF EXISTS `tbl_catoptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_catoptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  `option_value` varchar(100) NOT NULL,
  `time_delay` time NOT NULL,
  `parent_option_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(100) NOT NULL DEFAULT 'en',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_catoptions`
--

LOCK TABLES `tbl_catoptions` WRITE;
/*!40000 ALTER TABLE `tbl_catoptions` DISABLE KEYS */;
INSERT INTO `tbl_catoptions` VALUES (1,2,'Arabic','1','00:01:00',0,'en','2018-03-20 02:09:53',NULL),(2,2,'English','2','00:00:45',0,'en','2018-03-20 02:10:09','2018-03-20 02:17:53'),(3,2,'Service and spare parts','1','13:15:45',2,'en','2018-03-20 02:18:10',NULL),(4,2,'Automotive','2','13:15:15',2,'en','2018-03-20 02:18:28',NULL),(5,2,'Motorcycle','3','13:15:15',2,'en','2018-03-20 02:18:53',NULL),(6,2,'Power products','4','13:15:45',2,'en','2018-03-20 02:19:11',NULL),(7,2,'Customer service rep','9','13:15:00',2,'en','2018-03-20 02:19:30',NULL),(8,2,'Book a service','1','13:45:00',3,'en','2018-03-20 02:49:27',NULL),(9,2,'Spare parts','2','13:45:15',3,'en','2018-03-20 02:49:41',NULL),(10,2,'Safat alghanim showroom','1','13:45:15',4,'en','2018-03-20 02:50:54',NULL),(11,2,'shuwaikh showroom','2','13:45:15',4,'en','2018-03-20 02:51:49',NULL),(12,2,'service','3','13:45:45',4,'en','2018-03-20 02:52:01',NULL),(13,2,'customer service','9','13:45:00',4,'en','2018-03-20 02:52:14',NULL),(14,2,'repeat same menu','#','13:45:15',4,'en','2018-03-20 02:52:35',NULL),(15,1,'English','1','11:15:00',0,'en','2018-03-21 00:23:14',NULL),(16,1,'Arbic','2','11:45:15',0,'en','2018-03-21 00:54:56',NULL),(18,1,'Test4','8','12:00:45',16,'en','2018-03-21 01:07:12',NULL),(19,2,'Test','3435','12:00:15',2,'en','2018-03-21 01:07:38',NULL);
/*!40000 ALTER TABLE `tbl_catoptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_location`
--

DROP TABLE IF EXISTS `tbl_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `latitude` int(11) NOT NULL,
  `langitude` int(11) NOT NULL,
  `address` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_location`
--

LOCK TABLES `tbl_location` WRITE;
/*!40000 ALTER TABLE `tbl_location` DISABLE KEYS */;
INSERT INTO `tbl_location` VALUES (1,2,12,80,'MADURAI VEERAN NAGAR SH 68 Koothapakkam','2018-03-19 07:22:55','2018-03-19 07:23:41'),(2,1,38,-122,'75 Hagiwara Tea Garden Dr Golden Gate Park','2018-03-19 07:30:52',NULL),(3,2,13,80,'GST Rd Meenambakkam Chennai','2018-03-19 07:31:41',NULL),(4,2,12,80,'MADURAI VEERAN NAGAR SH 68 Koothapakkam','2018-03-19 08:17:04',NULL);
/*!40000 ALTER TABLE `tbl_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_service`
--

DROP TABLE IF EXISTS `tbl_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(11) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_description` text NOT NULL,
  `service_image` varchar(255) NOT NULL,
  `notification` text NOT NULL,
  `phone` varchar(100) NOT NULL,
  `sms` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `whatsapp` varchar(100) NOT NULL,
  `web` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL DEFAULT 'en',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_service`
--

LOCK TABLES `tbl_service` WRITE;
/*!40000 ALTER TABLE `tbl_service` DISABLE KEYS */;
INSERT INTO `tbl_service` VALUES (1,1,3,'BMW1','Test21','uploads/1/2018-03/bmw.jpeg','test23','0343 412 1323','23434324','test@gmail.com','23434324','23434','','en','2018-03-19 05:04:18','2018-03-19 05:25:41',1),(2,1,3,'Audi','Test1','uploads/1/2018-03/audi.png','test number','+509234324324','efrewrew','test@gmail.com','','','','en','2018-03-19 05:38:06','2018-03-19 05:38:23',1);
/*!40000 ALTER TABLE `tbl_service` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-21 15:32:52
