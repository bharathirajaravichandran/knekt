<?php

namespace App\Http\Middleware;

use Closure;

class CheckHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->server('HTTP_APIAUTHORIZATION');
        if (isset($header) && (trim($header) != '')) {
            $wsh = trim($header);            
            $header_values = base64_decode($header);
            $header_array = explode("|", $header_values);
            if (count($header_array) == 2) {
                $req_array = array();
                $input_array = array();
                foreach ($header_array as $k => $v) {
                    $prms = explode(":", $v);
                    if (!isset($prms[0]) || !isset($prms[1])) {
                        $error["error"] = "Empty Values in Header";
                    } else {
                        if ($prms[0] != 'Secret' && $prms[0] != 'Password') {
                            $error["error"] = "Invalid parameters";
                        } else {
                            $input_array[$prms[0]] = $prms[1];
                        }
                    }
                }

                
                if (isset($input_array) && !empty($input_array)) {
                   
                   $secret = trim($input_array["Secret"]);
					$password = trim($input_array["Password"]);
					if ($password == "knektplus#" && $secret=="a25la3RwbHVz") {
							$error["success"] = "Successfully Authenticated";
							return $next($request);
						}
						else
						{
							$error["error"] = "Invalid credentials";
						}
						
                } else {
                    $error["error"] = "Invalid parameters";
                }
            } else {
                $error["error"] = "Invalid Number of Header Values";
            }
        } else {
            $error['error'] = "No proper header set";
        } 
        return response()->json($error);  
        
        
    }
}
