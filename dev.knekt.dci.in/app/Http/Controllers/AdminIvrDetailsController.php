<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;

	use crocodicstudio\crudbooster\controllers\Controller;
	use Illuminate\Support\Facades\Storage;
	use Illuminate\Support\Facades\App;
	use Illuminate\Support\Facades\Mail;
	use Illuminate\Support\Facades\Route;
	use Illuminate\Support\Facades\Hash;
	use Illuminate\Support\Facades\Cache;
	use Illuminate\Support\Facades\Validator;
	use Illuminate\Support\Facades\PDF;
	use Maatwebsite\Excel\Facades\Excel;
	use CRUDBooster;
	use CB;
	use Schema;
	use App\Models\Cat_option;


	class AdminIvrDetailsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "option_name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "tbl_catoptions";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"COMPANY NAME","name"=>"company_id","join"=>"tbl_service,service_name"];
			$this->col[] = ["label"=>"SERVICE NAME","name"=>"option_name"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			if(isset($_REQUEST['parent_id']) && $_REQUEST['parent_id'] == '' ) {
				$this->form[] = ['label'=>'IVR LANGUAGE','name'=>'option_name','type'=>'select','validation'=>'required','width'=>'col-sm-2','dataenum'=>'English|English;Arabic|Arabic'];
		}
		else
		{
			$this->form[] = ['label'=>'Service Name','name'=>'option_name','type'=>'text','validation'=>'required','width'=>'col-sm-5'];
		}
			$this->form[] = ['label'=>'IVR  Value','name'=>'option_value','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-2'];
			
			$this->form[] = ['label'=>'Company Name','name'=>'company_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-5','datatable'=>'tbl_service,service_name'];
			$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'text','validation'=>'required|digits_between:8,11','width'=>'col-sm-3'];
			$this->form[] = ['label'=>'Parent Option Name','name'=>'parent_option_id','type'=>'select2','width'=>'col-sm-5','datatable'=>'tbl_catoptions,option_name'];
			$this->form[] = ['label'=>'Time Delay','name'=>'time_delay','type'=>'time','width'=>'col-sm-2','value' => '00:00:00'];

			$this->form[] = ['label'=>'token','name'=>'_token','type'=>'hidden','width'=>'col-sm-2',
			'value' => csrf_token()];
			$this->form[] = ['label'=>'trigger','name'=>'trigger','type'=>'hidden','width'=>'col-sm-2',
			'value' =>'' ];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form[] = ['label'=>'IVR LANGUAGE','name'=>'option_name','type'=>'select','validation'=>'required','dataenum'=>'English|English;Arabic|Arabic'];
			//$this->form[] = ['label'=>'IVR  Value','name'=>'option_value','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'text','validation'=>'required','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Name','name'=>'company_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tbl_service,service_name'];
			//$this->form[] = ['label'=>'Parent Option Name','name'=>'parent_option_id','type'=>'select2','width'=>'col-sm-9','datatable'=>'tbl_catoptions,option_name'];
			//$this->form[] = ['label'=>'Time Delay','name'=>'time_delay','type'=>'time','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	       $this->sub_module = array([
	       				'label'=>'SubIVR','path'=>'ivr_details','fore'=>'company_id,phone','foreign_key'=>'parent_option_id','button_color'=>'warning','button_icon'=>'fa fa-tags','parent_columns'=>'option_name,option_value,company_id']);


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	
	        $this->script_js = "
   							$(function() {                        


   								        $('#reset').on('click',function() {
										   $('.form-horizontal')[0].reset();
									}); 


									

									//remove flash message
                                   setTimeout(function() {
									    $('#successMessage').fadeOut('fast');
									}, 3000);
									//remove flash message


									

            var urls = '/get_company_phone';

              $(document).ready(function() {     

				$('#company_id').change(function() {
                 
                 	var company_id = $(this).val();
					$.ajax({
						type:'POST',
						url:  urls ,
						data: {
							    comp_id : company_id
						      },
						success: function(result) 
						{ 
								$('#phone').val(result) ;						
						}
					});				  

				});
			});		


                
         $('#form_popup').click(function()
          {  
              $('#category_tab').val($(this).parent().parent().attr('id'));              
              $('#cate_form').trigger('reset');
              $('.sucess_msg').html(''); $('.option_value_errMsg').html(''); 
          }); 

          $('.form_popup_subcate').click(function()
	          {   	           
	              $('#category_tab').val($(this).parent().parent().attr('id'));
	              $('#form_post').val('0');
	              
	          });   

	      $('.sub_close_popup').click(function()
	          {                
	              $('#form_post').val('0');
	              
	          });



	      $('.clone_popup').click(function()
	          {                
	             var company_id = $(this).attr('class').split(' ') ;
	             var company_phone = '';	            
	             if(typeof company_id[3]!='')
	             {     
	             	   var datas = company_id[3].split('_');	             	   
	             	   company_id = datas[1];
	             	   company_phone = datas[2];
	             	   $('#company_id').val(company_id);
	             	   $('#company_phone').val(company_phone);                         
	             }	           
	                   
	               
	          });

     $('.clone_close').click(function()
	          {          
	              $('#clone_service_name').val(''); $('#clone_company_name').val(''); 
          	      $('.clone_service_name_errMsg').html('') ; $('.clone_comapny_name_errMsg').html('') ;
	              
	          });
          

$('#clone_button').click(function(e)
          {     e.preventDefault();           	
          	   
          	   if($('#clone_service_name').val() ==''){ $('.clone_service_name_errMsg').css('color','red').html('Please Enter Service Name') ; }else{ $('.clone_service_name_errMsg').html('') ; }
          	   if($('#clone_company_name').val() ==''){ $('.clone_comapny_name_errMsg').css('color','red').html('Please Select Company Name') ; }else{  $('.clone_comapny_name_errMsg').html('') ; }
          	if($('#clone_service_name').val() =='' || $('#clone_company_name').val() =='')
	       	{   
	       		 return false;	              
	       	}

	       	           /* ajax clone function start */	
	       	            var action_url   = 'ajax_clone' ;                        
                        var token = $('input[name=_token]').val(); 
                        var option_name = $('#clone_service_name').val(); 
                        var new_company_id = $('#clone_company_name').val();                      
	       	       
	       	        $.ajax({
				        url : action_url ,
				        type: 'post',
				        dataType: 'json',
				        data:  { _token: token , new_company_id: new_company_id , option_name : option_name ,
				         old_company_id: $('#company_id').val()  } ,
				        success: function (response)
				        {	
				            $('.option_value_errMsg').html('');				        
					        $('.clone_service_name_errMsg').html('');				        
					        $('.clone_comapny_name_errMsg').html('');			
					        $('.clone_sucess_msg').html('Data was Clone Successfully');				        
					        setTimeout(function()
					          { 
					          	$('.clone_close').trigger('click');	

							  }, 3000);				            
					    },
				      error: function(jqXHR, textStatus, errorThrown)
				        { 
				             console.log(textStatus, errorThrown);
				        }

					});
	       	

          });






  /* main category form add method and validation check  */ 

         $('#add_cate_form').click(function(e)
          {     e.preventDefault();         	
               $('#option_name').val(); $('#option_value').val(); $('#company_name').val();$('#phone').val();$('#time_delay').val();
       if($('#option_name').val() ==''){ $('.option_name_errMsg').css('color','red').html('Please select option name') ; }
       if($('#option_value').val() ==''){ $('.option_value_errMsg').css('color','red').html('Please enter value') }      
       if($('#time_delay').val() ==''){ $('.time_delay_errMsg').css('color','red').html('Please enter time') }
       	if($('#option_name').val() =='' || $('#option_value').val() =='' || $('#phone').val() =='' || $('#time_delay').val() =='')
	       	{
	              return false;
	       	}

                        var action_url   = 'ajax_add_category' ;                        
                        var token = $('input[name=_token]').val(); 
                        var option_name = $('#option_name').val(); 
                        var option_value = $('#option_value').val();                        
                        var phone = $('#company_phone').val(); 
                        var time_delay = $('#time_delay').val(); 
	       	       
	       	        $.ajax({
				        url : action_url ,
				        type: 'post',
				        dataType: 'json',
				        data:  { _token: token , company_id: $('#company_id').val() , option_name: option_name , option_value:option_value,phone:phone ,time_delay:time_delay} ,
				        success: function (response)
				        {				
					        $('.sucess_msg').html('Successfully addedd');
					        $('.option_value_errMsg').html('');				        
					        setTimeout(function()
					          { 
					          	$('#form_popup').trigger('click');
							    

						  var data = response; 
					       $('#tab2 ul li').remove(); $('#tab3 ul li').remove(); $('#tab4 ul li').remove();
					       $('#tab5 ul li').remove(); $('#tab6 ul li').remove(); $('#tab7 ul li').remove();
					       $('#tab8 ul li').remove(); $('#tab9 ul li').remove(); $('#tab10 ul li').remove();
					       if(data =='') { $('#tab2 ul ').append('<li class=><a>No data</a></li>'); }    
					      $.each(data, function (i, item)
					      { 
	$('#tab2 ul ').append('<li onClick=tab2($(this)); class=><a class=language_'+item.id+'>'+item.option_name+'</a></li>');
	    			      });


							  }, 3000);				            
					    },
					     error: function(jqXHR, textStatus, errorThrown)
					      { 
					            console.log(textStatus, errorThrown);
					      }

					});

          });
/* main category form add method and validation check  */ 





/* subcategory  form add method and validation check  */ 

         $('#add_sub_form').click(function(e)
          {     e.preventDefault();         	
               
    if($('#form_post').val()== '0') 
    {
               $('#sub_option_name').val();
               $('#sub_option_value').val();               
               $('#sub_time_delay').val();
       if($('#sub_option_name').val() ==''){ $('.sub_option_name_errMsg').css('color','red').html('Please Enter Service Name') ; }
       if($('#sub_option_value').val() ==''){ $('.sub_option_value_errMsg').css('color','red').html('Please Enter Ivr Value') }      
       if($('#sub_time_delay').val() ==''){ $('.sub_time_delay_errMsg').css('color','red').html('Please enter time') }       	
 if($('#sub_option_name').val() =='' || $('#sub_option_value').val() =='' || $('#sub_time_delay').val() =='')
	       	{
	              return false;
	       	}

                        var action_url   = 'ajax_add_subcate' ;                        
                        var token = $('input[name=_token]').val(); 
                        var option_name = $('#sub_option_name').val(); 
                        var option_value = $('#sub_option_value').val();                        
                        var phone = $('#company_phone').val(); 
                        var time_delay = $('#sub_time_delay').val(); 
                        var tab = $('#category_tab').val();        	       
                        var parent_id = $('#subcategory').val();        	       
	       	        $.ajax({
				        url : action_url ,
				        type: 'post',
				        dataType: 'json',
				        data:  { _token: token , company_id: $('#company_id').val() , option_name: option_name , option_value:option_value,phone:phone ,time_delay:time_delay,tab:tab ,parent_id:parent_id } ,
				        success: function (response)
				        {				
					        $('.sub_sucess_msg').html('Successfully addedd');
					        $('.sub_option_name_errMsg').html('');				        
					        $('.sub_option_value_errMsg').html('');				        
					        $('.sub_time_delay_errMsg').html('');				        
					        setTimeout(function()
					          { 

									var dat_count = tab.substring(3,5);
									var add_dat_count = parseInt(dat_count) + parseInt(1);
						            for(i = dat_count ; i < 15 ; i++) 
						            { 
						                $('#tab'+i+' ul li').remove();
						            }					          	

					          	$('#close_popup').trigger('click');
					          	var data = response;
					       


					if(data =='') { $('#tab'+dat_count+' ul ').append('<li class=><a>No data</a></li>'); }
					var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';    
					      $.each(data, function (i, item)
					      { 

	$('#tab'+dat_count+' ul ').append('<li class=><a onClick=tab'+add_dat_count+'($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');

	    			      });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');

	    			   /* form reset */
	    			   $('#sub_cate_form').trigger('reset');
	    			   $('.sub_sucess_msg').html('');
	    			   /* form reset */


							  }, 3000);				            
					    },
					     error: function(jqXHR, textStatus, errorThrown)
					      { 
					            console.log(textStatus, errorThrown);
					      }

					});
	}else
	{
         //update data start
		                var urls  = 'ajax_update_data' ; 
		                var current_tab      = $('#category_tab').val() ;
		                var auto_incrementid = $('#subcategory').val() ;
		                var sub_option_name  = $('#sub_option_name').val() ;
		                var sub_option_value = $('#sub_option_value').val() ;
		                var sub_time_delay   = $('#sub_time_delay').val() ;
           $.ajax({
						type:'POST',
						url:  urls ,
						data: {
							    auto_incrementid : auto_incrementid , sub_option_name : sub_option_name, sub_option_value : sub_option_value, sub_time_delay : sub_time_delay,
						      },
						success: function(response) 
						{ 
                            $('.sub_sucess_msg').html('Updated Successfully');
					        $('.sub_option_name_errMsg').html('');				        
					        $('.sub_option_value_errMsg').html('');				        
					        $('.sub_time_delay_errMsg').html('');	
				setTimeout(function()
					          { 

								$('#form_post').val('0') ;
								var dat_count = current_tab.substring(3,5);
								var add_dat_count = parseInt(dat_count) + parseInt(1);
								$('#tab'+dat_count+' ul li').remove();		          	
					          	var data = response;
					if(data =='') { $('#tab'+dat_count+' ul ').append('<li class=><a>No data</a></li>'); }
					var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';    
					      $.each(data, function (i, item)
					      { 

	$('#tab'+dat_count+' ul ').append('<li class=><a onClick=tab'+add_dat_count+'($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');

	    			      });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');
	    			   $('#close_popup').trigger('click');	

 }, 3000);	

						}
					});				  


		//update data start


	}

          });

/* main subcategory form add method and validation check  */ 








               
 
     /* tab--2 */                
                                  
          $('#tab1 li').click(function(){             	
          	  
          	  var category_id = $(this).find('a').attr('class').split('_');
          	  $('#company_phone').val(category_id[2]);
          	  var category_id = category_id[1];           	   	  
          	  $('#company_id').val(category_id);
          	  $('#tab2').css('display','block');
			   if(category_id !='')
			       {                                    
                        var action_url   = 'ivr_data' ;                        
                        var token = $('input[name=_token]').val(); 
				        $.ajax({
				        url : action_url ,
				        type: 'post',
				        dataType: 'json',
				        data:  { _token: token , category_id: category_id , tab: 1 } ,
				        success: function (response)
				        {				
					       var data = response; 
					       $('#tab2 ul li').remove(); $('#tab3 ul li').remove(); $('#tab4 ul li').remove();
					       $('#tab5 ul li').remove(); $('#tab6 ul li').remove(); $('#tab7 ul li').remove();
					       $('#tab8 ul li').remove(); $('#tab9 ul li').remove(); $('#tab10 ul li').remove();
					       if(data =='') { $('#tab2 ul ').append('<li class=><a>No data</a></li>'); }    
					      $.each(data, function (i, item)
					      { 
	$('#tab2 ul ').append('<li onClick=tab2($(this)); class=><a class=language_'+item.id+'>'+item.option_name+'</a></li>');
	    			      });
					            
					      },
					     error: function(jqXHR, textStatus, errorThrown)
					      { 
					            console.log(textStatus, errorThrown);
					      }

						    });
		   
       }

                });


   			});







   /* tab--3 */
  function tab2(val)
            {               	
                var language_id   = val.find('a').attr('class').split('_');   
                var language_name = val.find('a').html();                 
                $('#subcategory').val($.trim(language_id[1]));
                $('#language').val($.trim(language_name)); 
                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab3').css('display','block');                
      	
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		        data:  { _token: token , company_id: company_id , language_id: language_id[1] , tab: 2 } ,
		        success: function (response)
		        {     
		        	var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';
		             var clone  = 'fa-clone'; 
		              var data = response;
		                   $('#tab3 ul li').remove(); $('#tab4 ul li').remove();
					       $('#tab5 ul li').remove(); $('#tab6 ul li').remove(); $('#tab7 ul li').remove();
					       $('#tab8 ul li').remove(); $('#tab9 ul li').remove(); $('#tab10 ul li').remove();
					        $('#tab11 ul li').remove();$('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					   if(data =='') { $('#tab3 ul ').append('<li class=><a>No data</a></li>'); } 
				      $.each(data, function (i, item)
				      {   
                           $('#tab3 ul ').append('<li class=><a onClick=tab3($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=subcategory_clone('+item.id+',$(this)); class=fa'+clone+' aria-hidden=true data-toggle=modal href=.modal_nnn></i><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');
	    			   $('.fafa-clone').removeClass('fafa-clone').addClass('fa fa-clone');
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }






  function tab3(val)
            {               	
                var subcategory_id   = val.attr('class').split('_');                              
                var subcategory_name = val.html(); 
                $('#subcategory').val($.trim(subcategory_id[1])); 
                $('#subcategory_name').val($.trim(subcategory_name));               

                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab4').css('display','block');     	
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		        data:  { _token: token , company_id: company_id , subcategory_id: subcategory_id[1] ,tab: 3 } ,
		        success: function (response)
		        {     
		              var data = response;		                   
		                   $('#tab4 ul li').remove();
					       $('#tab5 ul li').remove();
					       $('#tab6 ul li').remove();
					       $('#tab7 ul li').remove();
					       $('#tab8 ul li').remove();
					       $('#tab9 ul li').remove();
					       $('#tab10 ul li').remove();
					        $('#tab11 ul li').remove();$('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';
                    if(data =='') {  $('#tab4 ul').append('<li class=><a>No data</a></li>'); }  
				      $.each(data, function (i, item)
				      {   
                           $('#tab4 ul').append('<li class=><a onClick=tab4($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });
	    			$('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			$('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');	
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }




    function tab4(val)
            {               	
                var subcategory_id   = val.attr('class').split('_');                
                var subcategory_name = val.html(); 
                $('#subcategory').val($.trim(subcategory_id[1])); 
                $('#subcategory_name').val($.trim(subcategory_name));               

                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab5').css('display','block');     	 
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		       data:  { _token: token , company_id: company_id , subcategory_id: subcategory_id[1] ,tab: 4 } ,
		        success: function (response)
		        {     
		              var data = response;			                   
					       $('#tab5 ul li').remove();
					       $('#tab6 ul li').remove();
					       $('#tab7 ul li').remove();
					       $('#tab8 ul li').remove();
					       $('#tab9 ul li').remove();
					       $('#tab10 ul li').remove();
					        $('#tab11 ul li').remove();$('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					var pencil = 'fa-pencil';
		            var trash  = 'fa-trash';
                    if(data =='') {  $('#tab5 ul').append('<li class=><a>No data</a></li>'); }  
				      $.each(data, function (i, item)
				      {   
                           $('#tab5 ul').append('<li class=><a onClick=tab5($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });
	    			  $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			  $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');	
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }


  /* tab--3 */
  function tab5(val)
            {               	
                var subcategory_id   = val.attr('class').split('_');                
                var subcategory_name = val.html(); 
                $('#subcategory').val($.trim(subcategory_id[1])); 
                $('#subcategory_name').val($.trim(subcategory_name));               

                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab6').css('display','block');     	 
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		       data:  { _token: token , company_id: company_id , subcategory_id: subcategory_id[1] ,tab: 5 } ,
		        success: function (response)
		        {     
		        	var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';
		              var data = response;
		                   $('#tab6 ul li').remove(); $('#tab7 ul li').remove();
					       $('#tab8 ul li').remove(); $('#tab9 ul li').remove(); $('#tab10 ul li').remove();  $('#tab11 ul li').remove();$('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					   if(data =='') { $('#tab6 ul ').append('<li class=><a>No data</a></li>'); } 
				      $.each(data, function (i, item)
				      {   
                           $('#tab6 ul ').append('<li class=><a onClick=tab6($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }



  /* tab--3 */
  function tab6(val)
            {               	
                var subcategory_id   = val.attr('class').split('_');                
                var subcategory_name = val.html(); 
                $('#subcategory').val($.trim(subcategory_id[1])); 
                $('#subcategory_name').val($.trim(subcategory_name));               

                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab7').css('display','block');     	 
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		       data:  { _token: token , company_id: company_id , subcategory_id: subcategory_id[1] ,tab: 6 } ,
		        success: function (response)
		        {     
		        	var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';
		              var data = response;
		                   $('#tab7 ul li').remove();
					       $('#tab8 ul li').remove(); $('#tab9 ul li').remove(); 
					       $('#tab10 ul li').remove();
					        $('#tab11 ul li').remove();$('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					   if(data =='') { $('#tab7 ul ').append('<li class=><a>No data</a></li>'); } 
				      $.each(data, function (i, item)
				      {   
                           $('#tab7 ul ').append('<li class=><a onClick=tab7($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }


  /* tab--3 */
  function tab7(val)
            {               	
                var subcategory_id   = val.attr('class').split('_');                
                var subcategory_name = val.html(); 
                $('#subcategory').val($.trim(subcategory_id[1])); 
                $('#subcategory_name').val($.trim(subcategory_name));               

                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab8').css('display','block');     	 
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		       data:  { _token: token , company_id: company_id , subcategory_id: subcategory_id[1] ,tab: 7 } ,
		        success: function (response)
		        {     
		        	var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';
		              var data = response;
		                   $('#tab8 ul li').remove();
					       $('#tab9 ul li').remove();
					       $('#tab10 ul li').remove();
					        $('#tab11 ul li').remove();$('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					   if(data =='') { $('#tab8 ul ').append('<li class=><a>No data</a></li>'); } 
				      $.each(data, function (i, item)
				      {   
                           $('#tab8 ul ').append('<li class=><a onClick=tab8($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }




 /* tab--3 */
  function tab8(val)
            {               	
                var subcategory_id   = val.attr('class').split('_');                
                var subcategory_name = val.html(); 
                $('#subcategory').val($.trim(subcategory_id[1])); 
                $('#subcategory_name').val($.trim(subcategory_name));               

                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab9').css('display','block');     	 
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		       data:  { _token: token , company_id: company_id , subcategory_id: subcategory_id[1] ,tab: 8 } ,
		        success: function (response)
		        {     
		        	var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';
		              var data = response;	                   
					       $('#tab9 ul li').remove(); $('#tab10 ul li').remove();  $('#tab11 ul li').remove();$('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					   if(data =='') { $('#tab9 ul ').append('<li class=><a>No data</a></li>'); } 
				      $.each(data, function (i, item)
				      {   
                           $('#tab9 ul ').append('<li class=><a onClick=tab9($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }





function tab9(val)
            {               	
                var subcategory_id   = val.attr('class').split('_');                
                var subcategory_name = val.html(); 
                $('#subcategory').val($.trim(subcategory_id[1])); 
                $('#subcategory_name').val($.trim(subcategory_name));               

                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab10').css('display','block');     	 
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		       data:  { _token: token , company_id: company_id , subcategory_id: subcategory_id[1] ,tab: 9 } ,
		        success: function (response)
		        {     
		        	var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';
		              var data = response;	                   
					      $('#tab11 ul li').remove();$('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					   if(data =='') { $('#tab10 ul ').append('<li class=><a>No data</a></li>'); } 
				      $.each(data, function (i, item)
				      {   
                           $('#tab10 ul ').append('<li class=><a onClick=tab10($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }


function tab10(val)
            {               	
                 var subcategory_id   = val.attr('class').split('_');                
                var subcategory_name = val.html(); 
                $('#subcategory').val($.trim(subcategory_id[1])); 
                $('#subcategory_name').val($.trim(subcategory_name));               

                var action_url   = 'ivr_data' ;
                var token = $('input[name=_token]').val();                       
                var company_id = $('#company_id').val(); 
                $('#tab11').css('display','block');     	 
                $.ajax({
		        url : action_url ,
		        type: 'post',
		        dataType: 'json',
		       data:  { _token: token , company_id: company_id , subcategory_id: subcategory_id[1] ,tab: 10 } ,
		        success: function (response)
		        {     
		        	var pencil = 'fa-pencil';
		             var trash  = 'fa-trash';
		              var data = response;	    				      
					      $('#tab12 ul li').remove();
					      $('#tab13 ul li').remove();$('#tab14 ul li').remove();
					      $('#tab15 ul li').remove();
					   if(data =='') { $('#tab11 ul ').append('<li class=><a>No data</a></li>'); } 
				      $.each(data, function (i, item)
				      {   
                           $('#tab11 ul ').append('<li class=><a onClick=tab11($(this)); class=subcate_'+item.id+'>'+item.option_name+'</a><i onClick=data_edit('+item.id+',$(this)); class=fa'+pencil+' aria-hidden=true></i><i onClick=data_delete('+item.id+',$(this)); class=fa'+trash+' aria-hidden=true></i></li>');
	    			  });

	    			   $('.fafa-pencil').removeClass('fafa-pencil').addClass('fa fa-pencil');
	    			   $('.fafa-trash').removeClass('fafa-trash').addClass('fa fa-trash');
				            
				},
		       error: function(jqXHR, textStatus, errorThrown)
		        { 
		            console.log(textStatus, errorThrown);
		        }

			    });                             
                             
            }



 function data_edit(id,val)
            {   
            	var tab = $(val).parent().parent().parent().attr('id') ;            	
            	$('#form_post').val('1') ;            	
            	$('#category_tab').val(tab) ;
            	$('#subcategory').val(id) ;
                var action_url   = 'ajax_get_data' ;            	
            	$.ajax({
						type:'POST',
						url:  action_url ,
						data: {
							    category_id : id
						      },
						success: function(data) 
						{  
							  	$('#sub_option_name').val(data.option_name) ;
								$('#sub_option_value').val(data.option_value) ;
								$('#sub_time_delay').val(data.time_delay) ;
								$('#subcat_popup_form').trigger('click');						
						}
					});	 
				return false ;        	           	
            }


 function data_delete(id,val)
            {   
            	
            	
            	var result = confirm('Want to delete?');
					if (result)
					{		

					   var action_url   = 'ajax_delete_data' ;
                       var token = $('input[name=_token]').val();		   
					   $.ajax({
				        url : action_url ,
				        type: 'post',
				        dataType: 'json',
				        data:  { _token: token , delete_id : id } ,
				        success: function (response)
				        {	             
		                    	if(response ==1) { alert('Deleted Successfully') ;
		                    	$(val).parent().remove(); }
		                    	else{ alert('Data already Deleted') ; };		            
						},
				       error: function(jqXHR, textStatus, errorThrown)
				        { 
				            console.log(textStatus, errorThrown);
				        }

					    });  
					}          	           	
            	

            }

   							";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        /*$this->load_js = array(asset("js/common.js"),asset("js/scoop.min.js"));*/
	        $this->load_js = array();
	        
	        $this->load_css = array();
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


public function getIndex() {
		$this->cbLoader();
		$user  = CRUDBooster::me();
		$locale = $user->language;
		$module = CRUDBooster::getCurrentModule();

		if(!CRUDBooster::isView() && $this->global_privilege==FALSE) {
			CRUDBooster::insertLog(trans('crudbooster.log_try_view',['module'=>$module->name]));
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans('crudbooster.denied_access'));
		}

		if(Request::get('parent_table')) {
			//dd(Request::get('fore'));
			$parentTablePK = CB::pk(g('parent_table'));
			$data['parent_table'] = DB::table(Request::get('parent_table'))->where($parentTablePK,Request::get('parent_id'))->first();
			if(Request::get('foreign_key')) {
				$data['parent_field'] = Request::get('foreign_key');
			}else{
				$data['parent_field'] = CB::getTableForeignKey(g('parent_table'),$this->table);	
			}
			if(Request::get('fore')) {
				$data['paret'] = Request::get('fore');
			}

			if($parent_field) {
				foreach($this->columns_table as $i=>$col) {
					if($col['name'] == $parent_field || $col['name'] == $paret) {
						unset($this->columns_table[$i]);
					}
				}
			}
		}

		$data['table'] 	  = $this->table;
		$data['table_pk'] = CB::pk($this->table);
		$data['page_title']       = $module->name;
		$data['page_description'] = trans('crudbooster.default_module_description');
		$data['date_candidate']   = $this->date_candidate;
		$data['limit'] = $limit   = (Request::get('limit'))?Request::get('limit'):$this->limit;

		$tablePK = $data['table_pk'];
		$table_columns = CB::getTableColumns($this->table);
		$result = DB::table($this->table)->select(DB::raw($this->table.".".$this->primary_key));
		
		//for Language
		
		if(Request::get('parent_id')) {
			$table_parent = $this->table;
			$table_parent = CRUDBooster::parseSqlTable($table_parent)['table'];
			$result->where($table_parent.'.'.Request::get('foreign_key'),Request::get('parent_id'));
		}


		$this->hook_query_index($result);

		if(in_array('deleted_at', $table_columns)) {
			$result->where($this->table.'.deleted_at',NULL);
		}

		$alias            = array();
		$join_alias_count = 0;
		$join_table_temp  = array();
		$table            = $this->table;
		$columns_table    = $this->columns_table;
		foreach($columns_table as $index => $coltab) {

			$join = @$coltab['join'];
			$join_where = @$coltab['join_where'];
			$join_id = @$coltab['join_id'];
			$field = @$coltab['name'];
			$join_table_temp[] = $table;

			if(!$field) die('Please make sure there is key `name` in each row of col');

			if(strpos($field, ' as ')!==FALSE) {
				$field = substr($field, strpos($field, ' as ')+4);
				$field_with = (array_key_exists('join', $coltab))?str_replace(",",".",$coltab['join']):$field;
				$result->addselect(DB::raw($coltab['name']));
				$columns_table[$index]['type_data']   = 'varchar';
				$columns_table[$index]['field']       = $field;
				$columns_table[$index]['field_raw']   = $field;
				$columns_table[$index]['field_with']  = $field_with;
				$columns_table[$index]['is_subquery'] = true;
				continue;
			}

			if(strpos($field,'.')!==FALSE) {
				$result->addselect($field);
			}else{
				$result->addselect($table.'.'.$field);
			}

			$field_array = explode('.', $field);

			if(isset($field_array[1])) {
				$field = $field_array[1];
				$table = $field_array[0];
			}else{
				$table = $this->table;
			}

			if($join) {

				$join_exp     = explode(',', $join);

				$join_table  = $join_exp[0];
				$joinTablePK = CB::pk($join_table);
				$join_column = $join_exp[1];
				$join_alias  = str_replace(".", "_", $join_table);

				if(in_array($join_table, $join_table_temp)) {
					$join_alias_count += 1;
					$join_alias = $join_table.$join_alias_count;
				}
				$join_table_temp[] = $join_table;

				$result->leftjoin($join_table.' as '.$join_alias,$join_alias.(($join_id)? '.'.$join_id:'.'.$joinTablePK),'=',DB::raw($table.'.'.$field. (($join_where) ? ' AND '.$join_where.' ':'') ) );
				$result->addselect($join_alias.'.'.$join_column.' as '.$join_alias.'_'.$join_column);

				$join_table_columns = CRUDBooster::getTableColumns($join_table);
				if($join_table_columns) {
					foreach($join_table_columns as $jtc) {
						$result->addselect($join_alias.'.'.$jtc.' as '.$join_alias.'_'.$jtc);
					}
				}

				$alias[] = $join_alias;
				$columns_table[$index]['type_data']	= CRUDBooster::getFieldType($join_table,$join_column);
				$columns_table[$index]['field']      = $join_alias.'_'.$join_column;
				$columns_table[$index]['field_with'] = $join_alias.'.'.$join_column;
				$columns_table[$index]['field_raw']  = $join_column;

				@$join_table1  = $join_exp[2];
				@$joinTable1PK = CB::pk($join_table1);
				@$join_column1 = $join_exp[3];
				@$join_alias1  = $join_table1;

				if($join_table1 && $join_column1) {

					if(in_array($join_table1, $join_table_temp)) {
						$join_alias_count += 1;
						$join_alias1 = $join_table1.$join_alias_count;
					}

					$join_table_temp[] = $join_table1;

					$result->leftjoin($join_table1.' as '.$join_alias1,$join_alias1.'.'.$joinTable1PK,'=',$join_alias.'.'.$join_column);
					$result->addselect($join_alias1.'.'.$join_column1.' as '.$join_column1.'_'.$join_alias1);
					$alias[] = $join_alias1;
					$columns_table[$index]['type_data']	= CRUDBooster::getFieldType($join_table1,$join_column1);
					$columns_table[$index]['field']      = $join_column1.'_'.$join_alias1;
					$columns_table[$index]['field_with'] = $join_alias1.'.'.$join_column1;
					$columns_table[$index]['field_raw']  = $join_column1;
				}

			}else{

				$result->addselect($table.'.'.$field);
				$columns_table[$index]['type_data']	= CRUDBooster::getFieldType($table,$field);
				$columns_table[$index]['field']      = $field;
				$columns_table[$index]['field_raw']  = $field;
				$columns_table[$index]['field_with'] = $table.'.'.$field;
				//$result->where('language',$locale);
			}
		}

		if(Request::get('q')) {
			$result->where(function($w) use ($columns_table, $request) {
				foreach($columns_table as $col) {
						if(!$col['field_with']) continue;
						if($col['is_subquery']) continue;
						$w->orwhere($col['field_with'],"like","%".Request::get("q")."%");
				}
			});
		}

		if(Request::get('where')) {
			foreach(Request::get('where') as $k=>$v) {
				$result->where($table.'.'.$k,$v);
			}
		}

		$filter_is_orderby = false;
		if(Request::get('filter_column')) {

			$filter_column = Request::get('filter_column');
			$result->where(function($w) use ($filter_column,$fc) {
				foreach($filter_column as $key=>$fc) {

					$value = @$fc['value'];
					$type  = @$fc['type'];

					if($type == 'empty') {
						$w->whereNull($key)->orWhere($key,'');
						continue;
					}

					if($value=='' || $type=='') continue;

					if($type == 'between') continue;

					switch($type) {
						default:
							if($key && $type && $value) $w->where($key,$type,$value);
						break;
						case 'like':
						case 'not like':
							$value = '%'.$value.'%';
							if($key && $type && $value) $w->where($key,$type,$value);
						break;
						case 'in':
						case 'not in':
							if($value) {
								$value = explode(',',$value);
								if($key && $value) $w->whereIn($key,$value);
							}
						break;
					}


				}
			});

			foreach($filter_column as $key=>$fc) {
				$value = @$fc['value'];
				$type  = @$fc['type'];
				$sorting = @$fc['sorting'];

				if($sorting!='') {
					if($key) {
						$result->orderby($key,$sorting);
						$filter_is_orderby = true;
					}
				}

				if ($type=='between') {
					if($key && $value) $result->whereBetween($key,$value);
				}else{
					continue;
				}
			}
		}

		if($filter_is_orderby == true) {
			$data['result']  = $result->paginate($limit);

		}else{
			if($this->orderby) {
				if(is_array($this->orderby)) {
					foreach($this->orderby as $k=>$v) {
						if(strpos($k, '.')!==FALSE) {
							$orderby_table = explode(".",$k)[0];
							$k = explode(".",$k)[1];
						}else{
							$orderby_table = $this->table;
						}
						$result->orderby($orderby_table.'.'.$k,$v);
					}
				}else{
					$this->orderby = explode(";",$this->orderby);
					foreach($this->orderby as $o) {
						$o = explode(",",$o);
						$k = $o[0];
						$v = $o[1];
						if(strpos($k, '.')!==FALSE) {
							$orderby_table = explode(".",$k)[0];
						}else{
							$orderby_table = $this->table;
						}
						$result->orderby($orderby_table.'.'.$k,$v);
					}
				}
				$data['result'] = $result->paginate($limit);
			}else{
				$data['result'] = $result->orderby($this->table.'.'.$this->primary_key,'desc')->paginate($limit);
			}
		}

		$data['columns'] = $columns_table;

		if($this->index_return) return $data;

		//LISTING INDEX HTML
		$addaction     = $this->data['addaction'];

		if($this->sub_module) {
			foreach($this->sub_module as $s) {
				$table_parent = CRUDBooster::parseSqlTable($this->table)['table'];
				$addaction[] = [
					'label'=>$s['label'],
					'icon'=>$s['button_icon'],
					'url'=>CRUDBooster::adminPath($s['path']).'?parent_table='.$table_parent.'&parent_columns='.$s['parent_columns'].'&parent_columns_alias='.$s['parent_columns_alias'].'&parent_id=['.(!isset($s['custom_parent_id']) ? "id": $s['custom_parent_id']).']&return_url='.urlencode(Request::fullUrl()).'&foreign_key='.$s['foreign_key'].'&label='.urlencode($s['label']).'&fore='.$s['fore'],
					'color'=>$s['button_color'],
                                        'showIf'=>$s['showIf']
				];
			}
		}
		
		$mainpath      = CRUDBooster::mainpath();
		$orig_mainpath = $this->data['mainpath'];
		$title_field   = $this->title_field;
		$html_contents = array();
		$page = (Request::get('page'))?Request::get('page'):1; 
		$number = ($page-1)*$limit+1; 
		foreach($data['result'] as $row) {
			$html_content = array();

			if($this->button_bulk_action) {		

				$html_content[] = "<input type='checkbox' class='checkbox' name='checkbox[]' value='".$row->{$tablePK}."'/>";
			}

			if($this->show_numbering) {
				$html_content[] = $number.'. ';
				$number++;
			}

			foreach($columns_table as $col) {
		          if($col['visible']===FALSE) continue;		          

		          $value = @$row->{$col['field']};
		          $title = @$row->{$this->title_field};
		          $label = $col['label'];

		          if(isset($col['image'])) {
			            if($value=='') {			              
			              $value = "<a  data-lightbox='roadtrip' rel='group_{{$table}}' title='$label: $title' href='".asset('vendor/crudbooster/avatar.jpg')."'><img style='margin-left: 25px' width='40px' height='40px' src='".asset('vendor/crudbooster/avatar.jpg')."'/></a>";
			            }else{
							$pic = (strpos($value,'http://')!==FALSE)?$value:asset($value);				            
				            $value = "<a data-lightbox='roadtrip'  rel='group_{{$table}}' title='$label: $title' href='".$pic."'><img style='margin-left: 25px' width='40px' height='40px' src='".$pic."'/></a>";
			            }			            
		          }

		          if(@$col['download']) {
			            $url = (strpos($value,'http://')!==FALSE)?$value:asset($value).'?download=1';
			            if($value) {
			            	$value = "<a class='btn btn-xs btn-primary' href='$url' target='_blank' title='Download File'><i class='fa fa-download'></i> Download</a>";
			            }else{
			            	$value = " - ";
			            }
		          }

		            if($col['str_limit']) {
		            	$value = trim(strip_tags($value));
		            	$value = str_limit($value,$col['str_limit']);
		            }

		            if($col['nl2br']) {
		            	$value = nl2br($value);
		            }

		            if($col['callback_php']) {
		              foreach($row as $k=>$v) {
		              		$col['callback_php'] = str_replace("[".$k."]",$v,$col['callback_php']);
		              }
		              @eval("\$value = ".$col['callback_php'].";");
		            }

		            //New method for callback
			        if(isset($col['callback'])) {
			        	$value = call_user_func($col['callback'],$row);
			        }


		            $datavalue = @unserialize($value);
					if ($datavalue !== false) {
						if($datavalue) {
							$prevalue = [];
							foreach($datavalue as $d) {
								if($d['label']) {
									$prevalue[] = $d['label'];
								}
						    }
						    if(count($prevalue)) {
						    	$value = implode(", ",$prevalue);
						    }
						}
					}

		          $html_content[] = $value;
	        } //end foreach columns_table


	      if($this->button_table_action):

	      		$button_action_style = $this->button_action_style;
	      		$html_content[] = "<div class='button_action' style='text-align:right'>".view('crudbooster::components.action',compact('addaction','row','button_action_style','parent_field'))->render()."</div>";

          endif;//button_table_action


          foreach($html_content as $i=>$v) {
          	$this->hook_row_index($i,$v);
          	$html_content[$i] = $v;
          }

	      $html_contents[] = $html_content;
		} //end foreach data[result]


 		$html_contents = ['html'=>$html_contents,'data'=>$data['result']];

		$data['html_contents'] = $html_contents;

		return view("crudbooster::default.index",$data);
	}


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        if(!Request::get('foreign_key')) {
	            $query->where('parent_option_id',0);
	        }
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    	
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
		    if($postdata['company_id'] == '') {
			    $parent_id = $postdata['parent_option_id'];
			    $company_id = DB::table('tbl_catoptions')->where('id',$parent_id)->pluck('company_id')->first();
			    $postdata['company_id'] = $company_id;
		    }
                  //check if exists language option
                $user = DB::table('tbl_catoptions')
                              ->where('company_id', $postdata['company_id'])
                              /*->where('option_name','!=',$postdata['option_name'])*/
                              ->where('option_value',$postdata['option_value'])                             
                              ->where('parent_option_id',0)                             
                              ->count();                   
                       if($user !=0 )
                       {  
                       	    /*dd('already');exit;*/
                             $to       =  $_SERVER['HTTP_REFERER'];                     
                             $message  =  "already exists please enter another one language value";                     
                             $type     =  "success" ;  
                        Session::flash('alert-warning', 'already exists please enter another one language value');    
                        CRUDBooster::redirect($to,$message,$type);
                             exit;
                       }   
                     /*  dd('new');exit;*/
                 //check if exists language option

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

        public function get_company_phone(Request $request)
	{   
		$cat = DB::table('tbl_service')->where('id',$request::input('comp_id'))->select('phone')->first();	
		return response()->json($cat->phone);
 						
	}

	public function ivr_upload_form(Request $request)
	{   
		

		      $file = Request::file('ivr_upload') ;
		    
      if($file->getMimeType() == 'application/octet-stream')
      {	  


$data = [];
    Excel::filter('chunk')->load($file)->chunk(1000, function ($results) use (&$data) {
        foreach ($results as $row) {
            $data['company_id'][] = $row->toArray()['company_id'];
            $data['option_value'][] = $row->toArray()['option_value'];
        }
    }, $shouldQueue = false);
    return $data;

dd('exit');

      	/*$excel = [];
            Excel::load($file, function($reader) use (&$excel) {
                $objExcel = $reader->getExcel();
                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //  Loop through each row of the worksheet in turn
                for ($row = 1; $row <= $highestRow; $row++)
                {
                    //  Read a row of data into an array
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                        NULL, TRUE, FALSE);

                    $excel[] = $rowData;
                }
            });
dd($excel); exit;
            foreach ($excel as $key => $value) 
            {  
            	foreach ($value as $key1 => $value1) 
                {
            	   dd($value); exit;
                }
            }*/

    
	 
		$destinationPath = 'uploads';
        $file->move($destinationPath,$file->getClientOriginalName());
       /* $file_content = fopen("uploads/".$file->getClientOriginalName(),"r");
		while($line = fgetcsv($file_content))
		{
		   if(!empty($line[0]) && !empty($line[1]))
				DB::table('pincodes')->insert(['hub_id' => $hub_id,'location' => $line[0],'pincode' => $line[1], 'status' => 1]);
	    }
	    fclose($file_content);*/
		
		if(file_exists("uploads/".$file->getClientOriginalName()))
			unlink("uploads/".$file->getClientOriginalName());	  
		
		dd('sucess');
		    
      }
      else
      	dd('not here');
		return Redirect::back()->withErrors(['Please upload a valid CSV file.']);






 						
	}


	    //By the way, you can still create your own method in here... :) 


 public function ajax_clone(Request $request)
 { 
 	   if($_POST)
 	   { 
 	   	  $value = Cat_option::where('company_id',$request::input('old_company_id'))->get();
 	   	  
 	   	  if($value->count() !=0)
 	   	    {   
                foreach($value as $val)
                    {
                           $company_name_id = DB::table('tbl_service')
                                             ->where('status',1)
                                             ->where('id',$request::input('new_company_id'))
                                             ->first();

                          $data = new Cat_option();
                          $data->company_id   = $request::input('new_company_id');
                          $data->option_name  = $val->option_name ;
                          $data->option_value = $val->option_value ;
                          $data->phone        = $company_name_id->phone ;
                          $data->time_delay   = $val->time_delay ;
                          /*$data->parent_option_id = $val->parent_option_id ;*/
                          $data->language     = $val->language ;
                          $data->created_at   = $val->created_at ;
                          $data->updated_at   = $val->updated_at ;
                          $data->save();

                          

                          $datas = '1' ;
                          return response()->json($datas);

                    }
 	   	    }else
 	   	    {
                    $datas = '0' ;
                    return response()->json($datas);
 	   	    }
         
 	   }
        
 }

 public function ajax_delete_data(Request $request)
 { 
 	   if($_POST)
 	   {
          $value = Cat_option::find($request::input('delete_id'));
          if($value)
          {
                $value->delete();
                $data = '1' ;
          }else
          {
                $data = '0' ;
          }              
	     return response()->json($data);

 	   }
        
 }

 public function ajax_update_data(Request $request)
 { 
 	   if($_POST)
 	   {  
 	   	     	  
          $value = Cat_option::find($request::input('auto_incrementid'));
          if($value)
          {
                $value->option_name  = $request::input('sub_option_name') ;
                $value->option_value = $request::input('sub_option_value') ;
                $value->time_delay   = '00:'.$request::input('sub_time_delay') ;
                $value->save();
                $data = '1' ;
          } 
           $data = DB::table('tbl_catoptions')
	         ->where('company_id',$value->company_id)	         
	         ->where('parent_option_id',$value->parent_option_id)	
	         ->get();	                  
	     return response()->json($data);

 	   }
        
 }


public function ajax_add_category(Request $request)
	{ 
		   if($_POST)
		   {
              $cat_value = new Cat_option;
              $cat_value->company_id        = $request::input('company_id');
              $cat_value->option_name       = $request::input('option_name');
              $cat_value->option_value      = $request::input('option_value');
              $cat_value->phone             = $request::input('phone');
              $cat_value->time_delay        = '00:'.$request::input('time_delay');
              $cat_value->parent_option_id  =  0 ;
              $cat_value->language          = 'en' ;
              $cat_value->save();

              $data = DB::table('tbl_catoptions')
	         ->where('company_id',$request::input('company_id'))	         
	         ->where('parent_option_id',0)	
	         ->get();	          
    	     return response()->json($data);

		   }
            
	}



public function ajax_add_subcate(Request $request)
	{ 
		   if($_POST)
		   {  		   	  
              $cat_value = new Cat_option;
              $cat_value->company_id        = $request::input('company_id');
              $cat_value->option_name       = $request::input('option_name');
              $cat_value->option_value      = $request::input('option_value');
              $cat_value->phone             = $request::input('phone');
              $cat_value->time_delay        = '00:'.$request::input('time_delay');
              $cat_value->parent_option_id  =  $request::input('parent_id');
              $cat_value->language          = 'en' ;
              $cat_value->save();

              $data = DB::table('tbl_catoptions')
	         ->where('company_id',$request::input('company_id'))	         
	         ->where('parent_option_id',$request::input('parent_id'))	
	         ->get();	          
    	     return response()->json($data);

		   }
            
	}

public function ajax_get_data(Request $request)
	{ 
		   if($_POST)
		   {  		   	  
              $data = Cat_option::find($request::input('category_id'));
              $da   = explode(':',$data->time_delay);             
              $data->time_delay = $da[1].':'.$da[2];              
    	      return response()->json($data); 
		   }
            
	}




    public function ivr_data(Request $request)
	{  

		if($request::input('tab') == 1)
		{    
             $data = DB::table('tbl_catoptions')
	         ->where('company_id',$request::input('category_id'))	         
	         ->where('parent_option_id',0)	
	         ->get();	          
    	     return response()->json($data);	
		}
		elseif($request::input('tab') == 2)
		{
             $data = DB::table('tbl_catoptions')
	         ->where('company_id',$request::input('company_id'))	         
	         ->where('parent_option_id',$request::input('language_id'))	         
	         ->where('parent_option_id','!=',0)	
	         ->get();	                
    	    return response()->json($data);
		}
		elseif($request::input('tab') == 3)
		{
             $data = DB::table('tbl_catoptions')
	         ->where('company_id',$request::input('company_id'))	         
	         ->where('parent_option_id',$request::input('subcategory_id'))	         
	         ->where('parent_option_id','!=',0)	
	         ->get();	                
    	    return response()->json($data);
		}
		elseif($request::input('tab') == 4)
		{
             $data = DB::table('tbl_catoptions')
	         ->where('company_id',$request::input('company_id'))	         
	         ->where('parent_option_id',$request::input('subcategory_id'))	         
	         ->where('parent_option_id','!=',0)	
	         ->get();	                
    	    return response()->json($data);
		}
		else
		{  
			        		 
	        $data = DB::table('tbl_catoptions')
	         ->where('company_id',$request::input('company_id'))	         
	         ->where('parent_option_id',$request::input('subcategory_id'))	         
	         ->where('parent_option_id','!=',0)	
	         ->get();	        
    	    return response()->json($data);
		}		
		  			
	}


}
