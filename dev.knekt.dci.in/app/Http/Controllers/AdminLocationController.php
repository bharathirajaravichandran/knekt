<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Illuminate\Foundation\Validation\ValidatesRequests;
	use Illuminate\Support\Facades\Redirect;

	class AdminLocationController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "company_id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "tbl_location";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"COMPANY NAME","name"=>"company_id","join"=>"tbl_service,service_name"];
			$this->col[] = ["label"=>"ADDRESS","name"=>"address"];
			$this->col[] = ["label"=>"LATITUDE","name"=>"latitude"];
			$this->col[] = ["label"=>"LANGITUDE","name"=>"langitude"];
			
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Company Name','name'=>'company_id','type'=>'select2','validation'=>'required','width'=>'col-sm-5','datatable'=>'tbl_service,service_name','datatable_where'=>'status != 0'];
			$this->form[] = ['label'=>'Address','name'=>'address','type'=>'googlemaps','width'=>'col-sm-10','latitude'=>'latitude','longitude'=>'langitude'];
			# END FORM DO NOT REMOVE THIS LINE     

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Company Name','name'=>'company_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'tbl_service,service_name'];
			//$this->form[] = ['label'=>'Address','name'=>'address','type'=>'googlemaps','latitude'=>'latitude','longitude'=>'langitude'];
			//// $this->form[] = ['label'=>'Latitude','name'=>'latitude','type'=>'hidden','width'=>'col-sm-10'];
			//// $this->form[] = ['label'=>'Langitude','name'=>'langitude','type'=>'hidden','width'=>'col-sm-10'];
			//// $this->form[] = ['label'=>'Address','name'=>'address','type'=>'googlemaps','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	       $this->script_js = "
   							 $(function() {
   							 	        $('#reset').on('click',function() {
										   $('.form-horizontal')[0].reset();
									});
									
									//autocomplete form stop action when enter whout select 
				    				$('#input-search-autocomplete-address').blur(function( event ) {				    				
				    				  event.preventDefault();  
                                               });
                                      //autocomplete form stop action when enter whout select 
                                     
                   $(document).keypress(function(e) {
					    if(e.which == 13) {					        
					        $('.btn-primary').click();				        
					    }
                    });
                               //remove flash message
                                   setTimeout(function() {
									    $('#successMessage').fadeOut('fast');
									}, 3000);
									//remove flash message

                             $(document).ready(function() {      
                          $('form').submit(function () {
						    // Get the Login Name value and trim it 
						    var name      = $('#select2-company_id-container').val();
						    var address   = $('#address').val();  
						    var latitude  = $('#input-latitude-address').val(); 
						    var longitude = $('#input-longitude-address').val(); 

						    // Check if empty of not
						    if (address === '' || latitude === '' || longitude === '' ) {					       
						        alert('please browse your address');			        
						        return false;
						    }						   
                          });
                        });




   							 });";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	    	 $company_id = array();    	
	    	foreach ($query->get() as $key => $value) {	    	
	    		$result = DB::table('tbl_location')->where('id',$value->id)->get();	    		
	    		foreach ($result as $companykey => $company) {
	    			$company_id[]    = DB::table('tbl_service')->where('id',$company->company_id)->where('status',1)->first()->id;   			
	    		}
	    		
	    	}	    	    
	    $query->whereIn('company_id',array_filter($company_id));   
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata)
	     {        
	         //Your code here	      
	        if($postdata['latitude'] == '' || $postdata['langitude'] == '' )
                       {     
                       	     $to       =  $_SERVER['HTTP_REFERER'];                     
                             $message  =  "your address not found";                     
                             $type     =  "warning" ;  
                             Session::flash('alert-warning', 'your address not found');                    
                             CRUDBooster::redirectBack($message,$type)->with('message', 'Message sent!');
                             exit;
                       }
                      $user = DB::table('tbl_location')
                              ->where('company_id', $postdata['company_id'])
                              ->where('address', $postdata['address']) 
                              ->count();
                              
                       if($user !=0 )
                       {  
                             $to       =  $_SERVER['HTTP_REFERER'];                     
                             $message  =  "address already exists";                     
                             $type     =  "success" ;             
                             Session::flash('alert-warning', 'Record already exists');       
                             CRUDBooster::redirectBack($message,$type)->with('message', 'Message sent!');
                             exit;
                       }   
                                       
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here                  
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}
