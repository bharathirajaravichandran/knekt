<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDbooster;
use App\User;

class AdminCmsUsersController extends \crocodicstudio\crudbooster\controllers\CBController {


	public function cbInit() {
		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->table               = 'cms_users';
		$this->primary_key         = 'id';
		$this->title_field         = "name";
		$this->button_action_style = 'button_icon';	
		$this->button_import 	   = FALSE;	
		$this->button_export 	   = FALSE;	
		$this->button_filter = false;
		$this->button_show = false;
		$this->add_lable = "Add User";		
		# END CONFIGURATION DO NOT REMOVE THIS LINE
		$user = CRUDBooster::me();
		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = array();
		$this->col[] = array("label"=>"NAME","name"=>"name"); 
		$this->col[] = array("label"=>"EMAIL","name"=>"email");
		// $this->col[] = array("label"=>"Privilege","name"=>"id_cms_privileges","join"=>"cms_privileges,name");
		$this->col[] = array("label"=>"PHOTO","name"=>"photo","image"=>1);	
		$this->col[] = array('label'=>'STATUS','name'=>'status');	
		# END COLUMNS DO NOT REMOVE THIS LINE


		# START FORM DO NOT REMOVE THIS LINE
		$this->form = array(); 		
		$this->form[] = array("label"=>"Name","name"=>"name",'width'=>'col-sm-4','required'=>true,'validation'=>'required|alpha_spaces|min:3');

		$this->form[] = array("label"=>"Email","name"=>"email",'width'=>'col-sm-4','required'=>true,'type'=>'email','validation'=>'required|email|unique:cms_users,email,'.CRUDBooster::getCurrentId());

		$this->form[] = array("label"=>"Phone",'width'=>'col-sm-4','type'=>'tel','name'=>'phone','placeholder'=>"Enter your number",'validation'=>'numeric','autocomplete'=>"off");
				
		$this->form[] = array("label"=>"Photo","name"=>"photo",'width'=>'col-sm-4',"type"=>"upload","help"=>"Recommended resolution is 200x200px",'filemanager_type'=>'image');

		if($user->id_cms_privileges == 1)	{
		 if($user->id_cms_privileges != 1)											
			$this->form[] = array("label"=>"Privilege","name"=>"id_cms_privileges",'width'=>'col-sm-4',"type"=>"select","datatable"=>"cms_privileges,name",'datatable_where'=>'is_superadmin != 1','required'=>true);
		else	
			$this->form[] = array("label"=>"Privilege","name"=>"id_cms_privileges",'width'=>'col-sm-4',"type"=>"select","datatable"=>"cms_privileges,name",'required'=>true);	
			}	
		//if($user->id_cms_privileges == 1)			
		if(request()->segment(3) != 'edit')
		 {
		  $this->form[] = array("label"=>"Password",'width'=>'col-sm-3','required'=>true,"name"=>"password",'placeholder'=>"............","type"=>"password",'autocomplete'=>"off");
		    // "help"=>"Please leave empty if not change",
	     }else
	     {
		   $this->form[] = array("label"=>"Password",'width'=>'col-sm-2',"name"=>"password",'placeholder'=>"............","type"=>"password",'autocomplete'=>"off");
	     }

		$this->form[] = array("label"=>"Language",'width'=>'col-sm-2','name'=>'language','validation'=>'required','type'=>'select',"dataenum"=>"en|English;ar|Arabic");

		$this->form[] = array("label"=>"Status",'width'=>'col-sm-2','name'=>'status','type'=>'select','required'=>true,"dataenum"=>"1|Active;0|InActive");
		# END FORM DO NOT REMOVE THIS LINE
		# 
		# 
		$this->button_selected = array(
	        	['label'=>'InActive','icon'=>'fa fa-times','name'=>'deactive'],
                ['label'=>'Active','icon'=>'fa fa-check','name'=>'active']
	        	);
		$this->script_js = "
  							  $(function() {
  							  var val =  $('#status').val();
									if(val == '')
										$('#status').val('1');
       							   $('#phone').intlTelInput({
									 initialCountry : 'kw', 
									 hiddenInput: 'full_phone',
									 utilsScript:'http://dev.knekt.dci.in/js/utils.js'

       							   });
       							   // on blur: validate
									$('#phone').change(function() {
									 // reset();
									  if ($.trim($('#phone').val())) {
									    if ($('#phone').intlTelInput('isValidNumber')) {
									    	  $('#form-group-phone').removeClass('has-error');
									    } else {
									     $('#form-group-phone').addClass('has-error');
									    }
									  }
									});
       							   $('#close').on('click',function() {
									$('#photo').val('');
       							});

                       $(document).ready(function() 
                       {
							/*$('#photo').inputFileText( { text: 'Upload / drag & drop' } );*/
		               });

                    var pathArray = window.location.pathname.split( '/' );
                    var segment_1 = pathArray[3];  
                       if(segment_1 == 'add')
                           {
                                  jQuery(document).ready(function($){
								    $('input[name=phone]').attr('maxlength','8');								    
								  });
				                  $('#form').submit(function(){ 				                  	
                               if($('#phone').val().length != 0)
                               {				                   
							      if($('#phone').val().length > 7 )
				                        {  
				                           $('#form-group-phone').removeClass('has-error');                           
				                        }else
				                        {                        	
				                        	$('#form-group-phone').addClass('has-error');
				                        	return false;
				                        } 
				                }  
				                    }); 
                      	   }
                       else{
                                jQuery(document).ready(function($){
								    $('input[name=phone]').attr('maxlength', '9');								  
								  });
				                  $('#form').submit(function(){  
				                   if($('#phone').val().length != 0)
                                {                                
							      if($('#phone').val().length > 8 )
				                        {  
				                           $('#form-group-phone').removeClass('has-error');                           
				                        }else
				                        {                        	
				                        	$('#form-group-phone').addClass('has-error');
				                        	return false;
				                        }   
				                }
				                    });  
				            }					             
                       

                       /*$('#language').change(function()
                         {  
                            if($(this).val() == 'ar')
                            {
                               $('.control-label').eq(0).text('اسم *');
                               $('.control-label').eq(1).text('البريد الإلكتروني *');
                               $('.control-label').eq(2).text('هاتف *');
                               $('.control-label').eq(3).text('صورة فوتوغرافية');
                               $('.control-label').eq(4).text('امتياز *');
                               $('.control-label').eq(5).text('كلمه السر *');
                               $('.control-label').eq(6).text('لغة *');
                               $('.control-label').eq(7).text('الحالة *');
                            }
                            else
                            {
                               $('.control-label').eq(0).text('name *');
                               $('.control-label').eq(1).text('Email *');
                               $('.control-label').eq(2).text('Phone *');
                               $('.control-label').eq(3).text('Photo');
                               $('.control-label').eq(4).text('Privilege *');
                               $('.control-label').eq(5).text('Password *');
                               $('.control-label').eq(6).text('Language *');
                               $('.control-label').eq(7).text('status *');

                            }
                         
                       });*/
                       

       							 $('#reset').on('click',function() {
										   $('.form-horizontal')[0].reset();
									});
									$('.toggle-one').bootstrapToggle({
			    				 	 on: 'Active',
			     					 off: 'InActive'
    							});
				    				$('.toggle-one').change(function() {
				    					var td = $(this).closest('tr');
				      					var record_value = td.find('.checkbox').val();
				      					$.ajax({
									            type: 'POST',
									            url: '/user/update',
									            data: {id: record_value},
									            success: function( msg ) {
									            	console.log('Status Updated Successfully');
									            }
				        				});
				      					
				    				});				    				
				    				
				    				$('.fa-times-circle').text('Remove').css('color','red').css('cursor', 'pointer');
				    				
               				lightbox.option({
									  'maxWidth' : 350,
									  'maxHeight': 350,
									  'fitImagesInViewport' : true,
									  'positionFromTop':200
									})				    				
       							});
								";
			$this->load_js = array(asset("js/intlTelInput.js"),
	    							asset("js/data.js"),
	    							asset("js/jquery-input-file-text.js"),
	    							asset("js/bootstrap-toggle.min.js"));
		 $this->load_css = array(asset("css/intlTelInput.css"),
	    							asset("css/bootstrap-toggle.min.css"));
				
	}

	public function update(Request $request){
 		$cat = User::find($_POST['id']);
 		$cat->status = !$cat->status;
 		$cat->save();
 		echo 'sucess';die;


 	}
 	 public function hook_before_add(&$postdata)
 	    {        
	     if(empty($postdata['photo'])) 
             {
                    $postdata['photo'] = 'uploads/default_image/avatar.png';
             } 	 
         $postdata['user_type'] = 'web';       	
	     $postdata['phone']     = $_REQUEST['full_phone'];
	    }
	      public function hook_after_edit($id) {
	        //Your code here 
	    	$phone = $_REQUEST['full_phone'];
	    	$company = \App\User::find($id);
	    	$company->phone = ($phone) ? $phone : '';
	    	$company->save();
	    }
	 public function actionButtonSelected($id_selected,$button_name) {

	        if($button_name == 'active') {
        		DB::table($this->table)->whereIn('id',$id_selected)->update(['status'=>'1']);
    			}
    		if($button_name == 'deactive') {
        		DB::table($this->table)->whereIn('id',$id_selected)->update(['status'=>'0']);
    			}
	    }

	 public function hook_query_index(&$query) {
	        $user= CRUDBooster::me();
	       if($user->id_cms_privileges != 1)
	 			$query->where('cms_users.id','!=',1);

}
public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    	if($column_index == 4) {

	    		if($column_value == 0)
	    			$content = '<input class="toggle-one"  type="checkbox" >';
	    		else
	    			$content = '<input class="toggle-one" checked type="checkbox" >';
	    		$column_value = $content;
	    	}
	    }
	     

	public function getProfile()
	 {
		$this->button_addmore = FALSE;
		$this->button_cancel  = FALSE;
		$this->button_show    = FALSE;			
		$this->button_add     = FALSE;
		$this->button_delete  = FALSE;	
		$this->hide_form 	  = ['id_cms_privileges'];

		$data['page_title'] = trans("crudbooster.label_button_profile");
		$data['row']        = CRUDBooster::first('cms_users',CRUDBooster::myId());		
		$this->cbView('crudbooster::default.form',$data);				
	 }
	 
	 public function user_delete($id='NULL')
	 {
		$cat = User::find($id)->delete();
		Return redirect('admin/users');
 						
	 }
	 
}
