<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Company;
	class AdminCompanyController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "service_name";
			$this->limit = "20";
			$this->orderby = "updated_at,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "tbl_service";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Company Name","name"=>"service_name"];
			// $this->col[] = ["label"=>"PARENT CATEGORY","name"=>"parent_category_id","join"=>"tbl_category,category_name"];
			// $this->col[] = ["label"=>"SUB CATEGORY","name"=>"sub_cat_id","join"=>"tbl_category,category_name"];
			$this->col[] = ["label"=>"COMPANY IMAGE","name"=>"service_image","image"=>true];
			$this->col[] = ["label"=>"PHONE","name"=>"phone"];
			$this->col[] = ["label"=>"SMS","name"=>"sms"];
			$this->col[] = ["label"=>"EMAIL","name"=>"email"];
			$this->col[] = ["label"=>"WHATSAPP","name"=>"whatsapp"];
			$this->col[] = ['label'=>'STATUS','name'=>'status'];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Category','name'=>'parent_category_id','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-4','dataenum'=>'|Please select a Category','datatable'=>'tbl_category,category_name','datatable_where'=>'parent_category_id = 0','datatable_where'=>'status = 1'];
			$this->form[] = ['label'=>'Sub Category','name'=>'sub_cat_id','type'=>'select','width'=>'col-sm-4','datatable'=>'tbl_category,category_name','datatable_where'=>'parent_category_id != 0','parent_select'=>'parent_category_id'];

			$this->form[] = ['label'=>'Company Name','name'=>'service_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-4'];
			$this->form[] = ['label'=>'Company Description','name'=>'service_description','type'=>'textarea','width'=>'col-sm-6'];
			$this->form[] = ['label'=>'Company Image','name'=>'service_image','type'=>'upload','validation'=>'required','width'=>'col-sm-4','help'=>'Drag&Drop or Upload Your File'];
			
			$this->form[] = ['label'=>'Notification','name'=>'notification','type'=>'textarea','width'=>'col-sm-6']; 
			$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'tel','validation'=>'required|digits_between:8,11','width'=>'col-sm-3'];
			$this->form[] = ['label'=>'Sms Number','name'=>'sms','type'=>'text','validation'=>'digits_between:8,11','width'=>'col-sm-3'];
			$this->form[] = ['label'=>'Whatsapp Number','name'=>'whatsapp','type'=>'text','validation'=>'digits_between:8,11','width'=>'col-sm-3'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'LiveChat Url','name'=>'web','type'=>'text','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Instagram Url','name'=>'linkedin','type'=>'text','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Website','name'=>'website','type'=>'text','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'select','width'=>'col-sm-2','dataenum'=>'1|Active;0|Inactive'];
			# END FORM DO NOT REMOVE THIS LINE 

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Company Name','name'=>'service_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Description','name'=>'service_description','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Image','name'=>'service_image','type'=>'upload','validation'=>'required','width'=>'col-sm-10','help'=>'Drag&Drop or Upload Your File'];
			//$this->form[] = ['label'=>'Parent Category','name'=>'parent_category_id','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tbl_category,category_name','datatable_where'=>'parent_category_id = 0'];
			//$this->form[] = ['label'=>'Sub Category','name'=>'sub_cat_id','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tbl_category,category_name','datatable_where'=>'parent_category_id != 0','parent_select'=>'parent_category_id'];
			//$this->form[] = ['label'=>'Notification','name'=>'notification','type'=>'textarea','validation'=>'required','width'=>'col-sm-9','placeholder'=>'You can only enter the number only'];
			//$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'tel','validation'=>'required','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Sms','name'=>'sms','type'=>'text','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			//$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'LiveChat','name'=>'web','type'=>'text','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Whatsapp','name'=>'whatsapp','type'=>'text','width'=>'col-sm-9'];
			//$this->form[] = ['label'=>'Instagram','name'=>'linkedin','type'=>'text','width'=>'col-sm-9'];
			//$this->form[] = ['label'=>'Status','name'=>'status','type'=>'select','width'=>'col-sm-10','dataenum'=>'1|Active;0|Inactive'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        // $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = "
   							 $(function() {
   							 	
   							 	var val =  $('#status').val();
									if(val == '')
										$('#status').val('1');
       							   $('#phone').intlTelInput({
									 initialCountry : 'kw', 
									 hiddenInput: 'full_phone',
									 utilsScript:'http://dev.knekt.dci.in/js/utils.js'

       							   });
       							   $('#sms').intlTelInput({
									 initialCountry : 'kw', 
									 hiddenInput: 'full_sms',
									 utilsScript:'http://dev.knekt.dci.in/js/utils.js'

       							   });
       							   $('#whatsapp').intlTelInput({
									 initialCountry : 'kw', 
									 hiddenInput: 'full_whatsapp',
									 utilsScript:'http://dev.knekt.dci.in/js/utils.js'

       							   });
       							   $('#close').on('click',function() {
									$('#service_image').val('');
       							});
       							 $('#reset').on('click',function() {
										   $('.form-horizontal')[0].reset();
									});
									$('.toggle-one').bootstrapToggle({
			    				 	 on: 'Active',
			     					 off: 'InActive'
    							});
				    				$('.toggle-one').change(function() {
				    					var td = $(this).closest('tr');
				      					var record_value = td.find('.checkbox').val();
				      					$.ajax({
									            type: 'POST',
									            url: '/company/update',
									            data: {id: record_value},
									            success: function( msg ) {
									            	console.log('Status Updated Successfully');
									            }
				        				});
				      					
				    				});
				    				$('.fa-times-circle').text('Remove').css('color','red').css('cursor', 'pointer');
				    				
				    				lightbox.option({
									  'maxWidth' : 400,
									  'maxHeight': 400,
									  'fitImagesInViewport' : true,
									  'positionFromTop':200
									})
   							 });
							";

            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array(asset("js/intlTelInput.js"),
	    							asset("js/data.js"),
	    							asset("js/bootstrap-toggle.min.js"));
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        $this->button_selected = array(
	        	['label'=>'InActive','icon'=>'fa fa-times','name'=>'deactive'],
                ['label'=>'Active','icon'=>'fa fa-check','name'=>'active']
	        	);
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array(asset("css/intlTelInput.css"),
	    							asset("css/bootstrap-toggle.min.css"));
	        
	        
	    }

	     public function actionButtonSelected($id_selected,$button_name) {
	     	error_reporting(E_ALL);ini_set('display_errors','On');
	        if($button_name == 'active') {
        		DB::table($this->table)->whereIn('id',$id_selected)->update(['status'=>'1']);
    			}
    		if($button_name == 'deactive') {
        		DB::table($this->table)->whereIn('id',$id_selected)->update(['status'=>'0']);
    			}
	    }
	public function update(Request $request){
 		$cat = Company::find($_POST['id']);
 		$cat->status = !$cat->status;
 		$cat->save();
 		echo 'sucess';die;


 	}

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    // public function actionButtonSelected($id_selected,$button_name) {
	    //     //Your code here
	            
	    // }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {	
	    $category_id = $subcategory_id = array();    	
	    	foreach ($query->get() as $key => $value) {
	    		$result = DB::table('tbl_service')->where('id',$value->id)->where('status',1)->get();
	    		foreach ($result as $categorykeys => $category) {
	    			$category_id[]    = DB::table('tbl_category')->where('id',$category->parent_category_id)->where('status',1)->first()->id;
	    			$subcategory_id[] = DB::table('tbl_category')->where('id',$category->sub_cat_id)->where('status',1)->where('parent_category_id','!=',0)->first()->id;
	    		}
	    		
	    	}
	    $query->whereIn('parent_category_id',array_filter($category_id));
	    $query->whereIn('sub_cat_id',array_filter($subcategory_id));   	
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    	if($column_index == 7) {

	    		if($column_value == 0)
	    			$content = '<input class="toggle-one"  type="checkbox" >';
	    		else
	    			$content = '<input class="toggle-one" checked type="checkbox" >';
	    		$column_value = $content;
	    	}
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	     /*$postdata['phone'] = $_REQUEST['full_phone'];
	     $postdata['sms'] = $_REQUEST['full_sms'];
	     $postdata['whatsapp'] = $_REQUEST['full_whatsapp'];*/	
	     $postdata['phone'] = $_REQUEST['phone'];
	     $postdata['sms'] = $_REQUEST['sms'];
	     $postdata['whatsapp'] = $_REQUEST['whatsapp'];     

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here	       
	       
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 	        
	    	$phone = $_REQUEST['full_phone'];
	    	$company = \App\Company::find($id);
	    	//$company->phone = $phone;
	    	//$company->sms = $_REQUEST['full_sms'];
	    	//$company->whatsapp = $_REQUEST['full_whatsapp'];
	    	$company->phone = $_REQUEST['phone']; 
	    	$company->sms = $_REQUEST['sms'];
	    	$company->whatsapp = $_REQUEST['whatsapp'];
	    	$company->save();
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        

	    }



	    //By the way, you can still create your own method in here... :) 


	}
