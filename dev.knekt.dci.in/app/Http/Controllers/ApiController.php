<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use App\Category;
use App\Company;
use App\User;
use App\Models\Company_location;
use App\Models\Country;
use App\Models\Cat_option;
use App\Models\Favourite;
use App\Models\Recentcategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\PDF;
use Maatwebsite\Excel\Facades\Excel;
use CRUDBooster;
use CB;
use Schema;
use DOMDocument;

class ApiController extends Controller
{
      //
      public function Signup(Request $request){  
           /*error_log(print_r($_REQUEST,true),3,'/var/www/html/error.log');*/ 
           if($request->filled('phone') && $request->filled('password'))
             {   
                $user_mobile_check = User::where('phone',$request->phone)->get();
                if($user_mobile_check->count() !=0){
                     $response['msg']    = config('resource.mobile_number_exists');
                     $response['status'] = config('resource.status_420');                   
                     return response()->json($response,config('resource.status_200')); }
                /*$check = createNewDirectory();*/               
                //profile upload start
           	    $user_data = new User();
                $user_data->phone             =  $request->phone;
                $user_data->password          =  bcrypt($request->password);
                $user_data->photo             = 'uploads/default_image/avatar.png';
                $user_data->user_type         = 'app';             
                $user_data->id_cms_privileges =  3 ;
                $user_data->status            =  1 ;
  			        $user_data->save();
               //profile upload end              
               $response['msg']    = config('resource.signup_success');
      			   $response['status'] = config('resource.status_200');
      			   $response['data']   = array_null_values_replace(User::find($user_data->id)->toArray());
  			       return response()->json($response,config('resource.status_200'));
             }else
             {
                $response['msg'] = config('resource.parameter_missing');
  			        $response['status'] = config('resource.status_420');
  			        return response()->json($response,config('resource.status_200'));
             }
      }

      public function Signin(Request $request){   
        /*error_log(print_r($_REQUEST,true),3,'/var/www/html/error.log');*/
           if($request->filled('phone') && $request->filled('password'))
             {    $phone = $request->phone ;
             	    $password = $request->password ;
                  $user_check = User::where('phone',$request->phone)->where('id_cms_privileges',3)->get();
                   if($user_check->count() !=0){
                   	 if(password_verify($password,$user_check->first()->password)){
                   	 	//login success
                      $response['status']    = config('resource.status_200');
                      $response['msg']     = config('resource.login_success');
                      $response['data']   = array_null_values_replace($user_check->first()->toArray());
                      return response()->json($response,config('resource.status_200'));
                   	 }else{
                      //login failed, password wrong
                      $response['msg']    = config('resource.password_wrong');
                      $response['status'] = config('resource.status_420');
                      return response()->json($response,config('resource.status_200'));
                   	 }
                  }else{ 
                     	//user not found
                   	  $response['msg']   = config('resource.user_doesnt_exists'); 
          					  $response['status'] = config('resource.status_420');
          					  return response()->json($response,config('resource.status_200'));
                  } 
              }else{
                      $response['msg']   = config('resource.parameter_missing');
        			        $response['status'] = config('resource.status_420');
        			        return response()->json($response,config('resource.status_200'));
                }
      }


      public function checkUserExist(Request $request){   
        /*error_log(print_r($_REQUEST,true),3,'/var/www/html/error.log');*/
           if($request->filled('phone'))
             {    $phone = $request->phone ;               
                  $user_check = User::where('phone',$request->phone)->where('id_cms_privileges',3)->get();
                   if($user_check->count() !=0){                   
                      //login success
                      $response['status']    = config('resource.status_200');
                      $response['msg']       = config('resource.user_exists');
                      $response['data']      = array_null_values_replace($user_check->first()->toArray());
                      return response()->json($response,config('resource.status_200'));                   
                   }else{ 
                      //user not found
                      $response['msg']   = config('resource.user_doesnt_exists'); 
                      $response['status'] = config('resource.status_420');
                      return response()->json($response,config('resource.status_200'));
                   }

             }else{
                $response['msg']   = config('resource.parameter_missing');
                $response['status'] = config('resource.status_420');
                return response()->json($response,config('resource.status_200'));
           }
      }

      public function Forgotpassword(Request $request){   
        if($request->filled('id') && $request->filled('password'))
             {    $password = $request->password ;           	    
                  $user_check = User::where('id',$request->id)->get();               
                   if($user_check->count() !=0){
                    $user_check = User::find($request->id);
                   	//password update
                   	$user_check->password = bcrypt($request->password); 
                   	$user_check->save();
                    $response['msg']    = config('resource.password_updated_successfully');
                    $response['status'] = config('resource.status_200');
                    $response['data']   = array_null_values_replace($user_check->toArray());
                    return response()->json($response,config('resource.status_200'));

                   }else{ 
                   	  //user not found
                   	  $response['msg']   = config('resource.record_not_found');
  					          $response['status'] = config('resource.status_420');
  					          return response()->json($response,config('resource.status_200'));
                   }

             }else{
                $response['msg']   = config('resource.parameter_missing');
        			  $response['status'] = config('resource.status_420');
        			  return response()->json($response,config('resource.status_200'));
           }
        }

      public function Profile(Request $request) { 
       if($request->filled('type'))
               {    
              if($request->type == 'get'){
                        $user_check = User::where('id',$request->user_id)->get();
    	                 if($user_check->count() !=0){
    	                 	//get data
                          $user_check = User::find($request->user_id);
    	                 	  $response['data']   = array_null_values_replace($user_check->toArray());
    						          $response['status'] = config('resource.status_200');
    						          return response()->json($response,config('resource.status_200'));
    	                 }else{ 
    	                 	  //user not found
    	                 	  $response['msg']   = config('resource.record_not_found');
    						          $response['status'] = config('resource.status_420');
    						          return response()->json($response,config('resource.status_200'));
    	                 }
              }elseif($request->type == 'update'){
                     //get data
              	     $user_check = User::where('id',$request->user_id)->get();
  	                 if($user_check->count() !=0){        	                 	  
  	                 	  //update function start 
                        $user_check = User::find($request->user_id);  
                         //mobile check start 
                        $mobile_check = User::where('phone',$request->phone)->whereNotIn('id',[$request->user_id])->get(); 
                         if($mobile_check->count() !=0){
                            $response['msg']    = config('resource.mobile_number_exists');
                            $response['status'] = config('resource.status_420');                   
                            return response()->json($response,config('resource.status_200'));
                             }
                             //mobile check end                            
                        $exclude_inputs = array('id_cms_privileges','photo','password','type','user_id');
                        foreach($request->all() as $key=>$value){
                              if(!in_array($key,$exclude_inputs))
                                  $user_check->$key = $value;
                           }
                         if($request->filled('password')){ 
                          $user_check->password = bcrypt($request->password);
                         }
                         if($request->filled('photo')){  
                              //old image delete start
                              $old_image = '' ;
                              $old_image = explode('/',$user_check->photo);
                              if($old_image[1] == 3){
                                     $old_image = $old_image[3];
                                  }
                                  //old image delete end
                            //new folder create(every month) 
                            $new_folder_create = createNewDirectory();                                   
                            //single image                   
                          $profile_images = file_upload($request->photo,date("Y-m"),$old_image);
                          $user_check->photo = $new_folder_create.'/'.$profile_images;                    
                        }  	                 	   
                   	    $user_check->save();
                        $response['msg']    = config('resource.updated_successfully');
                        $response['status'] = config('resource.status_200');
                        $response['data']   = array_null_values_replace($user_check->toArray());
                   	    return response()->json($response,config('resource.status_200'));
  	                 }else{ 
  	                 	  //user not found
  	                 	  $response['msg']    = config('resource.record_not_found');
  						          $response['status'] = config('resource.status_420');
  						          return response()->json($response,config('resource.status_200'));
  	                 }

              }else{
              	  //type parameter mis maching
                 $response['data']   = config('resource.type_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
              }
               }else{
                $response['data']   = config('resource.parameter_missing');
  			        $response['status'] = config('resource.status_420');
  			        return response()->json($response,config('resource.status_200'));
           }
      }

      public function GetCategories(Request $request){                  
            $category = Category::where('parent_category_id',0)->orderBy('created_at','DESC')->jsonPaginate();
            if($request->filled('search_key')){
               $category = Category::where('parent_category_id',0)->orderBy('created_at','DESC')->where('category_name','like','%'.$request->search_key.'%')->jsonPaginate();
            }
            if($category->count() !=0)
                 {   
                    //categories data  
                    $response['status'] = config('resource.status_200');           
                    $response['data']   = $category ;           
                    return response()->json($response,config('resource.status_200')); 
                 }else{
                    //no categories
                    $response['msg']   = config('resource.record_not_found');
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));
               }
      }

    public function GetSubcategories(Request $request){   
        if($request->filled('category_id') && $request->filled('user_id'))
             {
          //recent data set start
         if($request->filled('user_id'))
            {
                 $Recentcategory = Recentcategory::where('user_id',$request->user_id)
                                                  ->where('category_id',$request->category_id)
                                                  ->orderBy('updated_at','asc')
                                                  ->get();
                 if($Recentcategory->count() ==0)
                 {    //new create
                       $data = new Recentcategory();
                       $data->category_id = $request->category_id ;
                       $data->user_id     = $request->user_id ;
                       $data->created_at  = date('Y-m-d H:i:s');
                       $data->save(); 
                 }else{
                       $Recentcategory->first()->updated_at = date('Y-m-d H:i:s');
                       $Recentcategory->first()->save();
                 }
                 $count = Recentcategory::where('user_id',$request->user_id)->orderBy('updated_at','asc')->get();                     
                  if($count->count() > 15)
                 {                           
                     $deleteUs = Recentcategory::take($count->count())->skip(15)->orderBy('updated_at','DESC')->get();                
                     foreach($deleteUs as $deleteMe){
                      Recentcategory::where('id',$deleteMe->id)->delete();
                     }
                 }                      
            }                  
           //recent data set end

            //get subcategories
            if($request->type == 'subcategory')
            { 
                  $subcategory = Company::where('parent_category_id',$request->category_id)
                                    ->where('sub_cat_id',$request->subcategory_id)                      
                                    ->orderBy('created_at','DESC')
                                    ->get();                                   
                 if($request->filled('search_key')){
                      $subcategory = Company::where('parent_category_id',$request->category_id)
                                        ->where('sub_cat_id',$request->subcategory_id)                  
                                        ->where('service_name','like','%'.$request->search_key.'%')
                                        ->orderBy('created_at','DESC')
                                        ->get();
                  }
              if($subcategory->count() !=0)
                   {  
                      //categories data 
                      $response['status'] = config('resource.status_200');             
                      $response['data']   = $subcategory ;            
                      return response()->json($response,config('resource.status_200'));                    

                   }else{                  
                  //no categories
                  $response['msg']    = config('resource.record_not_found');
                  $response['status'] = config('resource.status_420');
                  return response()->json($response,config('resource.status_200'));
                 }
               //get subcategories
            }else{               
            $subcategory = Category::where('parent_category_id',$request->category_id)
                                ->where('parent_category_id','!=',0)
                                ->orderBy('created_at','DESC')
                                ->get();
         if($request->filled('search_key')){
              $subcategory = Category::where('parent_category_id',$request->category_id)
                                ->where('parent_category_id','!=',0)
                                ->where('category_name','like','%'.$request->search_key.'%')
                                ->orderBy('created_at','DESC')
                                ->get();
          }
        if($subcategory->count() !=0)
             {  
                //categories data 
                $response['status'] = config('resource.status_200');             
                $response['data']   = $subcategory ;            
                return response()->json($response,config('resource.status_200'));                    

             }else{
            //no categories
            $response['msg']    = config('resource.record_not_found');
            $response['status'] = config('resource.status_420');
            return response()->json($response,config('resource.status_200'));
           }
     }
          }else{
                $response['msg']   = config('resource.parameter_missing');
                $response['status'] = config('resource.status_420');
                return response()->json($response,config('resource.status_200'));
           }
      }

      public function GetCompanyData(Request $request)
      {   
        if($request->filled('company_id') && $request->filled('type'))
          {   
              if($request->type == 'get'){  
                     $find_company = Company::where('id',$request->company_id)->get();                   
                     if($find_company->count() ==0){
                        $response['msg']    = config('resource.record_not_found');
                        $response['status'] = config('resource.status_420');
                        return response()->json($response,config('resource.status_200'));
                     }
          $find_company->first()->longitude=getCompanyBranchData($find_company->first()->id,'langitude');      
          $find_company->first()->latitude=getCompanyBranchData($find_company->first()->id,'latitude');
          $find_company->first()->bookmark = getFavouriteStatus($find_company->first()->id,null,'companystatus');
                  $response['status'] = config('resource.status_200');  
                  $response['data']   = $find_company->first()->toArray() ;           
                  return response()->json($response,config('resource.status_200'));
              }elseif($request->type == 'branch'){            
              $Company_location = Company_location::where('company_id',$request->company_id)->jsonPaginate();
              if($request->filled('search_key')){
              $Company_location = Company_location::where('company_id',$request->company_id)->where('category_name','like','%'.$request->search_key.'%')->jsonPaginate();  
              }
              if($Company_location->count() !=0)
                 {  
                    $company_name_add = array();
                    //Company_location data    
                    foreach ($Company_location as $key => $value){
                                   # code...
                            $value->bookmark     = getFavouriteStatus($value->company_id,null,'companystatus');
                            $value->company_data = getCompanyData($value->company_id);                    
                            $company_name_add[]  = $value ;
                            }  
                  $response['status'] = config('resource.status_200');                 
                  $response['data']   = $Company_location ;           
                  return response()->json($response,config('resource.status_200'));
                 }else{
                    //no Company_location
                    $response['msg']    = config('resource.record_not_found');
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));
               }
             }else{  
                    $response['msg']    = config('resource.type_missing');
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));
                  }
          }else{
                $response['msg']    = config('resource.parameter_missing');
                $response['status'] = config('resource.status_420');
                return response()->json($response,config('resource.status_200'));
           }
      }

      /**
   * Undocumented function
   *
   * @param Request $request
   * @return void
   */
  public function getCountry(Request $request) {
    $country = Country::where('flag','!=','')->get();
    //dd($country);
    foreach($country as $countries)
    {
      $countries->flag = 'images/flags/'.$countries->flag;
      $coun[] = $countries;

    }
    $response['data'] = $coun;
    $response['status'] = config('resource.status_200');
    return response()->json($response,config('resource.status_200'));
      }

    /**
   * Undocumented function
   *
   * @param Request $request
   * @return void
   */
  public function GetCompanyKnekt(Request $request){
    if($request->filled('company_id'))
          {   
              if($request->type == 'get'){  
                     $find_company = Company::where('id',$request->company_id)->get();                   
                     if($find_company->count() ==0){
                        $response['msg']    = config('resource.record_not_found');
                        $response['status'] = config('resource.status_420');
                        return response()->json($response,config('resource.status_200'));
                     }        
          $find_company->first()->bookmark           = getFavouriteStatus($request->company_id,null,'companystatus');         
          $find_company->first()->phone_bookmark     = 0 ;         
          $find_company->first()->sms_bookmark       = 0 ;         
          $find_company->first()->email_bookmark     = 0 ;         
          $find_company->first()->whatsapp_bookmark  = 0 ;         
          $find_company->first()->bookmark_web       = 0 ;         
          $find_company->first()->website_web        = 0 ;         
                  $response['data']   = $find_company->first()->toArray() ;           
                  return response()->json($response,config('resource.status_200'));
              }elseif($request->type == 'call'){                
                  $find_company = Cat_option::where('company_id',$request->company_id)->get();                   
                     if($find_company->count() ==0){
                        $response['msg']    = config('resource.record_not_found');
                        $response['status'] = config('resource.status_420');
                        return response()->json($response,config('resource.status_200'));
                     }  
                     $company_data = Cat_option::where('company_id',$request->company_id)
                         ->where('parent_option_id',0)->select('id','option_name')->jsonPaginate();
                          $bookmark_add = array();
                          //Company_location data    
                          foreach ($company_data as $key => $value){                                      
                                        $value->bookmark     = getFavouriteStatus($request->company_id,null,'companystatus') ;                                               
                                        $bookmark_add[]  = $value ;
                                  }
                     $response['status'] = config('resource.status_200');               
                     $response['data']   = $company_data ;           
                     return response()->json($response,config('resource.status_200'));
              }elseif($request->type == 'subcate'){                
                     $find_company = Cat_option::where('company_id',$request->company_id)
                                            ->where('parent_option_id',$request->parent_option_id)
                                            ->get();                   
                     if($find_company->count() ==0){
                        $response['msg']    = config('resource.record_not_found');
                        $response['status'] = config('resource.status_420');
                        return response()->json($response,config('resource.status_200'));
                     }  
                     $company_data = Cat_option::where('company_id',$request->company_id)
                         ->where('parent_option_id',$request->parent_option_id)->select('id','option_name')->jsonPaginate();
                          $bookmark_add = array();
                          //Company_location data    
                          foreach ($company_data as $key => $value){                                      
                                        $value->bookmark     = getFavouriteStatus($request->company_id,null,'companystatus') ;                                               
                                        $bookmark_add[]  = $value ;
                                  } 
                     $response['status'] = config('resource.status_200');              
                     $response['data']   = $company_data ;           
                     return response()->json($response,config('resource.status_200'));
              }else{
                 $response['msg']    = config('resource.parameter_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
             }
            }else{
                 $response['msg']    = config('resource.parameter_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
             }
          }

    public function AddFavourite(Request $request){
      if($request->filled('company_id'))
            {   if($request->type == 'add' && $request->filled('user_id') && $request->filled('company_id')){
               if($request->filled('value')){
                $Favourite = Favourite::where('company_id',$request->company_id)->where('user_id',$request->user_id)->where('value',$request->value)->get();
               }else{
                $Favourite = Favourite::where('company_id',$request->company_id)->where('user_id',$request->user_id)->get();
               }
               if($Favourite->count() ==0){ 
                    $newFavourite = new Favourite();
                    $newFavourite->company_id   = $request->company_id;
                    $newFavourite->user_id      = $request->user_id;
                    $newFavourite->value        = ($request->value !='') ? $request->value : 'Novalue' ;
                    $newFavourite->save();
                $response['msg']    = config('resource.added_successfully');
                $response['status'] = config('resource.status_200');
                return response()->json($response,config('resource.status_200'));
                    
               }else{                
                $Favourite->first()->delete();
                $response['msg']    = config('resource.removed_successfully');
                $response['status'] = config('resource.status_200');
                return response()->json($response,config('resource.status_200'));
               } 
               }else
               { 
                $Favourite = Favourite::where('id',$request->favourite_id)->get();
                if($Favourite->count() !=0){
                  $Favourite->first()->delete();
                  $response['msg']    = config('resource.removed_successfully');
                  $response['status'] = config('resource.status_200');
                  return response()->json($response,config('resource.status_200'));
                }else{
                  $response['msg']    = config('resource.record_not_found');
                  $response['status'] = config('resource.status_420');
                  return response()->json($response,config('resource.status_200'));
                }        
               }          
            }else{ 
               $response['msg']    = config('resource.parameter_missing');
               $response['status'] = config('resource.status_420');
               return response()->json($response,config('resource.status_200'));
            }
        }

    public function Getfavourites(Request $request){
      if($request->filled('user_id'))
            {                
                $Favourite = Favourite::where('user_id',$request->user_id)->get();          
               if($Favourite->count() !=0){ 
                   $company_data =array();
                   foreach ($Favourite as $key => $value) {                                    
                                        $value->company_data = getCompanyData($value->company_id);
                                        $company_data = $value ;
                                     }                  
                  $response['data']    = $Favourite->toArray();
                  $response['status']  = config('resource.status_200');
                return response()->json($response,config('resource.status_200'));                  
               }else{                
                  $response['msg']    = config('resource.no_data');
                  $response['status'] = config('resource.status_420');
                  return response()->json($response,config('resource.status_200'));
               }           
            }else{ 
                 $response['msg']    = config('resource.parameter_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
            }
        }
        
    public function GetRecentCategory(Request $request){
      if($request->filled('user_id'))
            {                
                $Recentcategory = Recentcategory::where('user_id',$request->user_id)
                                                ->orderBy('updated_at','DESC')->jsonPaginate();          
               if($Recentcategory->count() !=0){ 
                   $category_data =array();
                   foreach ($Recentcategory as $key => $value) {                                    
                                        $value->category_data = getCategoryData($value->category_id);
                                        $category_data = $value ;
                                     }                  
                  $response['data']    = $Recentcategory->toArray();
                  $response['status']  = config('resource.status_200');
                return response()->json($response,config('resource.status_200'));                  
               }else{                
                  $response['msg']    = config('resource.no_data');
                  $response['status'] = config('resource.status_420');
                  return response()->json($response,config('resource.status_200'));
               }           
            }else{ 
                 $response['msg']    = config('resource.parameter_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
            }
        }  
}
