<?php
use App\Company;
use App\Models\Company_location;
use App\Models\Favourite;
use App\Category;

function file_upload($data,$type,$old_image){	
	$ProfileImagedate = md5(date('Y-m-d H:i:s'));
	$ProfileImageData = base64_decode($data);
	$profileImageFilename = $ProfileImagedate.rand().".png";
	$location = $type.'/'.$profileImageFilename;		
	 	if(file_exists(storage_path('app/uploads/3/'.$type.'/'.$old_image)))
     	{    		   
   		   @unlink(storage_path('app/uploads/3/'.$type.'/'.$old_image));
     	}	 
	$path = \Storage::disk('public')->put($location, $ProfileImageData);
	//\Storage::put($profileImageFilename, $ProfileImageData);
	return $profileImageFilename;
 }

 function createNewDirectory()
{
	$sCurrDate = date("Y-m"); //Current Date
	$sDirPath  = storage_path('app/uploads/3/'.$sCurrDate); //Specified Pathname
	$path      = 'uploads/3/'.$sCurrDate;		
	if(!file_exists($sDirPath))
   	{   		
	    	mkdir($sDirPath,0777,true);  
    }    
    return $path ;
}
function array_null_values_replace($result)
	{
          if(is_array($result))
          { 
            array_walk_recursive($result, function (&$item, $key)
             {  
               $item = null === $item ? '' : $item;
             });
            return $result;
          }                 
          else
           {  
                $r['Status'] = 'Failed';
                $r['message'] = "Failed";
                return response()->json($r);
           }
    }
function getCompanyColumn($id,$column_name)
  {   
    $find_dats = Company::where('id',$id)->get();
       if($find_dats->count() !=0){
    $data = Company::find($id);
    return $data->$column_name;
          }
          $data = '';
          return $data;
  }

function getCompanyData($id)
  {   
    $find_dats = Company::where('id',$id)->get();
       if($find_dats->count() !=0){
            $data = Company::find($id);
            return $data->toArray();
          }
          $data = '';
          return $data;
  }
function getCompanyBranchData($id,$column_name)
{   
  $find_data = Company_location::where('company_id',$id)->get();
     if($find_data->count() !=0){
          $data = Company_location::where('company_id',$id)->first();
          return $data->$column_name;
        }
        $data = 0;
        return $data;
}
function getFavouriteStatus($company_id,$value,$type)
{   
      if($type == 'companystatus')
      {
         $Favourite = Favourite::where('company_id',$company_id)->where('value','Novalue')->get();
         return $Favourite->count();
      }
  
}
function getCategoryData($id)
{   
  $find_dats = Category::where('id',$id)->get();
     if($find_dats->count() !=0){
          $data = Category::find($id);
          return $data->toArray();
        }
        $data = '';
        return $data;
}
 ?>