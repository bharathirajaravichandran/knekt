<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ ($page_title)?Session::get('appname').': '.strip_tags($page_title):"Admin Area" }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name='generator' content='CRUDBooster 5.4.0.1'/>
    <meta name='robots' content='noindex,nofollow'/>
    <link rel="shortcut icon" href="{{ CRUDBooster::getSetting('favicon')?asset(CRUDBooster::getSetting('favicon')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>        
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("vendor/crudbooster/assets/adminlte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{asset("vendor/crudbooster/assets/adminlte/font-awesome/css")}}/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{asset("vendor/crudbooster/ionic/css/ionicons.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("vendor/crudbooster/assets/adminlte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset("vendor/crudbooster/assets/adminlte/dist/css/skins/_all-skins.min.css")}}" rel="stylesheet" type="text/css" />
   @php
$user = CRUDBooster::me();
if($user) 
  app()->setlocale($user->language);
@endphp
    <!-- support rtl-->
    @if (App::getLocale() == 'ar')
        <link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css">
        <link href="{{ asset("vendor/crudbooster/assets/rtl.css")}}" rel="stylesheet" type="text/css" />
    @endif

    <link rel='stylesheet' href='{{asset("vendor/crudbooster/assets/css/main.css").'?r='.time()}}'/>

    <!-- load css -->
    <style type="text/css">
        @if($style_css)
            {!! $style_css !!}
        @endif
    </style>
    @if($load_css)
        @foreach($load_css as $css)
            <link href="{{$css}}" rel="stylesheet" type="text/css" />
        @endforeach
    @endif
    
    <style type="text/css">
        .dropdown-menu-action {left:-130%;}
        .btn-group-action .btn-action {cursor: default}
        #box-header-module {box-shadow:10px 10px 10px #dddddd;}
        .sub-module-tab li {background: #F9F9F9;cursor:pointer;}
        .sub-module-tab li.active {background: #ffffff;box-shadow: 0px -5px 10px #cccccc}
        .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {border:none;}
        .nav-tabs>li>a {border:none;}                
        .breadcrumb {
            margin:0 0 0 0;
            padding:0 0 0 0;
        }
        .form-group > label:first-child {display: block}        
    </style>




   @if(Request::segment(2) == 'ivr_details')
    <style type="text/css">
         #New_Tab .tab-pane.active { display:block !Important; }
#New_Tab .nav.nav-tabs li.active a { width:200px; background:#000; display:block; border-radius:0px; color:#ddd; border:1px solid #000; position:relative; }
#New_Tab .nav.nav-tabs li.active a:before { position:absolute; top:10px; right:0; content:'\f0da'; font-family:fontawesome;  }
#New_Tab .nav.nav-tabs li.active a { border-radius:0px; }
#New_Tab .nav.nav-tabs li a { color:#ddd; }
#New_Tab .nav.nav-tabs li { width:200px;   display:block; border-radius:0px; }
#New_Tab .nav>li>a:hover, .nav>li>a:focus { border-radius:0px; background:#444; border:1px solid #444; }
#New_Tab .left1 { float:left; width:200px; background:#323E51; }
#New_Tab .left2 { float:left; width:200px;  background:#323E51;  }
#New_Tab .left1 a { display:block; width:100%; padding:10px 15px; }
#New_Tab .New_Tab_left { width:200px; float:left; background:#1C292E; }
#New_Tab .New_Tab_right {  float:left; overflow-x:auto; max-width:700px;  }

#New_Tab .left2 .tab-pane.active { display:block !Important; padding:10px 15px; color:#ddd; }
#New_Tab { display:inline-block;  } 
.flow_gird  {  overflow-x: scroll;  white-space: nowrap;  width: 900px;  }
.flow_gird .nav.nav-tabs {  position: relative; width:180px;  float:left; display:inline-block; border-radius:0px; }
.flow_gird .nav.nav-tabs li {  display:block; border-radius:0px; float:none; position:relative; }
.flow_gird .nav.nav-tabs li a { display:block;  border-radius:0px; background:#222D32; display:block; border-radius:0px; color:#ddd; border:1px solid #ddd; position:relative; white-space: nowrap;  overflow: hidden;   text-overflow: ellipsis;  max-width: 200px;  padding-right: 52px; cursor:pointer;  }

/*.flow_gird  {overflow-x: scroll; white-space:nowrap; width: 900px;  }
.flow_gird .nav.nav-tabs {      width: 250px; float: none; border-radius: 0px;  display: inline-block; }
.flow_gird .nav.nav-tabs li { width:200px; display:block; float:left; border-radius:0px; }
.flow_gird .nav.nav-tabs li a { display:block;  border-radius:0px; background:#000; border-radius:0px; color:#ddd; border:1px solid #000; position:relative; }*/

.scoop-inner-navbar .select2.select2-container { display: none;  }

 #tab1 span { positon:relative; }
 #tab1 span:before { position:absolute; top:10px; right:0; content:'\f055'; font-family:fontawesome; width:20px; height:20px; cursor:pointer; }
.flow_gird .nav.nav-tabs span {     /* float: right; */
    position: absolute;
    right: -1px;
    color: #000;
    width: 20px;
    height: 20px;
    /* background: #fff; */
    z-index: 999;
    cursor: pointer;
    top: 3px;  }
.flow_gird .nav.nav-tabs span:before { color:#fff;  }
#sub_category_form .modal-dialog {  background: #fff;  padding: 20px 20px;  }
#sub_category_form .modal-dialog .left { width: 100%;  margin-bottom: 10px;     font-size: 14px; color: #444; font-weight: 600; }
#sub_category_form .modal-dialog .right { width: 100%;  margin-bottom: 10px; }
#sub_category_form .modal-dialog .left  input { height: 35px; width:100%; }
#sub_category_form .modal-dialog .right  input { height: 35px; width:100%; }
#sub_category_form .modal-dialog select { width: 200px; height: 35px;  }   


#category_form .modal-dialog {  background: #fff;  padding: 20px 20px; width:330px; }
#category_form .modal-dialog .left { width: 50%;  margin-bottom: 10px; }
#category_form .modal-dialog .right { width: 100%;  margin-bottom: 10px; }
#category_form .modal-dialog .left  input { height: 35px; width:100%; }
#category_form .modal-dialog .right  input { height: 35px; width:100%; }
#category_form .modal-dialog select { border:1px solid #ddd; width:100%; height: 35px;  }    

 /* popup form start */
.modal_nnn {
  position: fixed;
  top: 10%;
  left:0;
  right:0;
  z-index: 1050;
  width: 600px;
  margin: 0 auto;
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  outline: none;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
          background-clip: padding-box;
}

.modal_nnn.fade {
  top: -25%;
  -webkit-transition: opacity 0.3s linear, top 0.3s ease-out;
     -moz-transition: opacity 0.3s linear, top 0.3s ease-out;
       -o-transition: opacity 0.3s linear, top 0.3s ease-out;
          transition: opacity 0.3s linear, top 0.3s ease-out;
}

.modal_nnn.fade.in {
  top: 10%;
}

.modal_nnn-header {
  padding: 9px 15px;
  border-bottom: 1px solid #eee;
}

.modal_nnn-header .close {
  margin-top: 2px;
}

.modal_nnn-header h3 {
  margin: 0;
  line-height: 30px;
}

.modal_nnn-body {
  position: relative;
  max-height: 400px;
  padding: 15px;
  overflow-y: auto;
}

.modal_nnn-form {
  margin-bottom: 0;
}

.modal_nnn-footer {
  padding: 14px 15px 15px;
  margin-bottom: 0;
  text-align: right;
  background-color: #f5f5f5;
  border-top: 1px solid #ddd;
  -webkit-border-radius: 0 0 6px 6px;
     -moz-border-radius: 0 0 6px 6px;
          border-radius: 0 0 6px 6px;
  *zoom: 1;
  -webkit-box-shadow: inset 0 1px 0 #ffffff;
     -moz-box-shadow: inset 0 1px 0 #ffffff;
          box-shadow: inset 0 1px 0 #ffffff;
}

.modal_nnn-footer:before,
.modal_nnn-footer:after {
  display: table;
  line-height: 0;
  content: "";
}

.modal_nnn-footer:after {
  clear: both;
}

.modal_nnn-footer .btn + .btn {
  margin-bottom: 0;
  margin-left: 5px;
}

.modal_nnn-footer .btn-group .btn + .btn {
  margin-left: -1px;
}

.modal_nnn-footer .btn-block + .btn-block {
  margin-left: 0;
}


/* popup form start */



.modal_nn {
  position: fixed;
  top: 10%;
  left:0;
  right:0;
  z-index: 1050;
  width: 600px;
  margin: 0 auto;
  
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  outline: none;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
          background-clip: padding-box;
}

.modal_nn.fade {
  top: -25%;
  -webkit-transition: opacity 0.3s linear, top 0.3s ease-out;
     -moz-transition: opacity 0.3s linear, top 0.3s ease-out;
       -o-transition: opacity 0.3s linear, top 0.3s ease-out;
          transition: opacity 0.3s linear, top 0.3s ease-out;
}

.modal_nn.fade.in {
  top: 10%;
}

.modal_nn-header {
  padding: 9px 15px;
  border-bottom: 1px solid #eee;
}

.modal_nn-header .close {
  margin-top: 2px;
}

.modal_nn-header h3 {
  margin: 0;
  line-height: 30px;
}

.modal_nn-body {
  position: relative;
  max-height: 400px;
  padding: 15px;
  overflow-y: auto;
}

.modal_nn-form {
  margin-bottom: 0;
}

.modal_nn-footer {
  padding: 14px 15px 15px;
  margin-bottom: 0;
  text-align: right;
  background-color: #f5f5f5;
  border-top: 1px solid #ddd;
  -webkit-border-radius: 0 0 6px 6px;
     -moz-border-radius: 0 0 6px 6px;
          border-radius: 0 0 6px 6px;
  *zoom: 1;
  -webkit-box-shadow: inset 0 1px 0 #ffffff;
     -moz-box-shadow: inset 0 1px 0 #ffffff;
          box-shadow: inset 0 1px 0 #ffffff;
}

.modal_nn-footer:before,
.modal_nn-footer:after {
  display: table;
  line-height: 0;
  content: "";
}

.modal_nn-footer:after {
  clear: both;
}

.modal_nn-footer .btn + .btn {
  margin-bottom: 0;
  margin-left: 5px;
}

.modal_nn-footer .btn-group .btn + .btn {
  margin-left: -1px;
}

.modal_nn-footer .btn-block + .btn-block {
  margin-left: 0;
}


.tabbs li .fa.fa-pencil { float: :right; width: 20px;   padding-top: 4px;
    position: absolute;
     right: 36px; cursor: pointer;  top: 13px;
    z-index: 999 !important;
    color: #ddd; }
.tabbs li .fa.fa-trash { float: :right; width: 20px;   padding-top: 4px;
    position: absolute;
    right: 13px; cursor: pointer; top: 13px;
    z-index: 999 !important;
    color: #ddd; }
/**18.july**/
.flow_gird .nav.nav-tabs li a.active { background:#000; }	
#category_form .modal-dialog .left {
    width: 100%;
    margin-bottom: 7px;
    font-size: 14px;
    font-weight: 600;
    color: #444;
}
#category_form .modal-header .close {
    margin-top: -2px;
    position: absolute;
    top: -9px;
    right: -10px;
    background: #000;
    color: #fff;
    width: 25px;
    height: 25px;
    font-size: 13px;
    opacity: 1;
    border-radius: 50%;
}
#category_form .modal-dialog .right select { border:1px solid #ddd; }
#category_form .modal-dialog .right input {   height: 35px;
    width: 100%;
    text-align: left;
	padding:2px 10px !Important;
    display: block;
	border:1px solid #ddd;  -webkit-transition: all 0.30s ease-in-out;
  -moz-transition: all 0.30s ease-in-out;
  -ms-transition: all 0.30s ease-in-out;
  -o-transition: all 0.30s ease-in-out;
  outline: none; }
#category_form .modal-dialog .right input:focus {
  
	box-shadow: 0 0 5px rgba(81, 203, 238, 1);
  padding: 3px 0px 3px 3px;
  margin: 5px 1px 3px 0px;
  border: 1px solid rgba(81, 203, 238, 1);
}


#sub_category_form { width:100%;  }
#sub_category_form .modal-header .sub_close_popup {  margin-top: -2px;
    position: absolute;
    top: -9px;
    right: -10px;
    background: #000;
    color: #fff;
    width: 25px;
    height: 25px;
    font-size: 13px;
    opacity: 1;
    border-radius: 50%;  }
	
#sub_category_form 	#close_popup { color: #fff;  background-color: #d9534f;  border-color: #d43f3a; }
#sub_category_form 	#add_sub_form {     color: #fff;  background-color: #337ab7;  border-color: #2e6da4; }
#sub_category_form .modal-dialog {  background: #fff;  padding: 20px 20px;  width: 330px; }
#category_form  .modal-footer .btn.sub_close_popup { color: #fff;  background-color: #d9534f;  border-color: #d43f3a; }
.sucess_msg { color:green; font-size:14px; font-weight:600; display:block; padding:10px 0px; }
.sub_sucess_msg { color:green; font-size:14px; font-weight:600; display:block; padding:10px 0px; }
#sub_category_form .modal-dialog .right input { padding:2px 10px !Important; }
/**23.july**/
.flow_gird .tabbs {
    position: relative;
    width: 20%;
    float: none;
    display: inline-block !important;
    border-radius: 0px;
}	
.flow_gird .nav.nav-tabs li .fa-clone { position: absolute; right: 5px; top: 7px; z-index: 99999;  width: 20px; height: 20px; display: inline-block;  color: #fff; cursor:pointer; }
.flow_gird .tabbs .nav.nav-tabs li .fa-clone {     width: 20px;
    padding-top: 4px;
    position: absolute;
    right: 60px;
    cursor: pointer;
    top: 13px;
    z-index: 999 !important;
color: #ddd;  display: inline-block;  color: #fff; cursor:pointer; }

#clone_form .modal-dialog {
    background: #fff;
    padding: 20px 20px;
    width: 330px;
}	
#clone_form {   background: transparent;  border: 0; }	
#clone_form .modal-footer .btn.clone_close {
    color: #fff;
    background-color: #d9534f;
    border-color: #d43f3a;
}
#clone_form .modal-header .close {
    margin-top: -2px;
    position: absolute;
    top: -9px;
    right: -10px;
    background: #000;
    color: #fff;
    width: 25px;
    height: 25px;
    font-size: 13px;
    opacity: 1;
    border-radius: 50%;
}

#clone_form .modal-dialog .left {
    width: 100%;
    margin-bottom: 7px;
    font-size: 14px;
    font-weight: 600;
    color: #444;
}
	
#clone_form .modal-dialog .right select { border:1px solid #ddd;  height: 35px; outline:none;
    width: 100%;
    text-align: left;
	padding:2px 10px !Important;
    display: block; }
#clone_form .modal-dialog .right input {   height: 35px;
    width: 100%;
    text-align: left;
	padding:2px 10px !Important;
    display: block;
	border:1px solid #ddd;  -webkit-transition: all 0.30s ease-in-out;
  -moz-transition: all 0.30s ease-in-out;
  -ms-transition: all 0.30s ease-in-out;
  -o-transition: all 0.30s ease-in-out;
  outline: none; }
#clone_form .modal-dialog .right input:focus {
  
	box-shadow: 0 0 5px rgba(81, 203, 238, 1);
  padding: 3px 0px 3px 3px;
  margin: 5px 1px 3px 0px;
  border: 1px solid rgba(81, 203, 238, 1);
}	
.nav-tabs { border-bottom:0px; }
.flow_gird .tabbs {     vertical-align: top !Important; } 
	
@media only screen and (max-width:767px){ 	
#category_form .modal-dialog {
    background: #fff;
    padding: 20px 20px;
    width: 95%;
    margin: 0 auto;
    margin-top: 10%;
}
#clone_form .modal-dialog {
    background: #fff;
    padding: 20px 20px;
    width: 95%;
    margin: 0 auto;
    margin-top: 10%;
}
#sub_category_form .modal-dialog { width:100%; width:95%; margin:0 auto; margin-top:10%;  }
#clone_form { width:100%; z-index:99999; }

}

    </style>




    @endif





    @stack('head')
</head>
<body class="@php echo (Session::get('theme_color'))?:'skin-blue'; echo ' '; echo config('crudbooster.ADMIN_LAYOUT'); @endphp {{($sidebar_mode)?:''}}">
<div id='app' class="wrapper">    

    <!-- Header -->
    @include('crudbooster::header')

    <!-- Sidebar -->
    @include('crudbooster::sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <section class="content-header">
          <?php 
            $module = CRUDBooster::getCurrentModule();
          ?>
          @if($module)
          <h1>
            <!-- <i class='{{$module->icon}}'></i>  {{($page_title)?:$module->name}} -->&nbsp;&nbsp;
             
            <!--START BUTTON -->         
                                        
            @if(CRUDBooster::getCurrentMethod() == 'getIndex')
            @if($button_show)
            <a href="{{ CRUDBooster::mainpath().'?'.http_build_query(Request::all()) }}" id='btn_show_data' class="btn btn-sm btn-primary" title="{{trans('crudbooster.action_show_data')}}">
              <i class="fa fa-table"></i> {{trans('crudbooster.action_show_data')}}
            </a>
            @endif

            @if($button_add && CRUDBooster::isCreate())                          
            <a href="{{ CRUDBooster::mainpath('add').'?return_url='.urlencode(Request::fullUrl()).'&parent_id='.g('parent_id').'&parent_field='.$parent_field.'&paret='.$paret }}" id='btn_add_new_data' class="btn btn-sm btn-success" title="Add {{ $module->singular }}">
              <i class="fa fa-plus-circle"></i> Add {{ $module->singular }}
            </a>

            <!-- <form action="{{ url('admin/ivr_upload_form') }}" method="POST" id="ivr_upload_form" class="ivr-upload-form" enctype="multipart/form-data">
            {{ csrf_field() }}
              <input  type='file' id="ivr_upload" title="{{$form['label']}}" {{$required}} {{$readonly}} {{$disabled}} class='form-control' name="ivr_upload"/> 
             <input name="submit" value="Save" class="btn btn-success" type="submit"> 
            </form>    -->  

            @endif                          
            @endif

              
            @if($button_export && CRUDBooster::getCurrentMethod() == 'getIndex')
            <a href="javascript:void(0)" id='btn_export_data' data-url-parameter='{{$build_query}}' title='Export Data' class="btn btn-sm btn-primary btn-export-data">
              <i class="fa fa-upload"></i> {{trans("crudbooster.button_export")}}
            </a>
            @endif

            @if($button_import && CRUDBooster::getCurrentMethod() == 'getIndex')
            <a href="{{ CRUDBooster::mainpath('import-data') }}" id='btn_import_data' data-url-parameter='{{$build_query}}' title='Import Data' class="btn btn-sm btn-primary btn-import-data">
              <i class="fa fa-download"></i> {{trans("crudbooster.button_import")}}
            </a>
            @endif

            <!--ADD ACTIon-->
             @if(count($index_button))                          
               
                    @foreach($index_button as $ib)
                     <a href='{{$ib["url"]}}' id='{{str_slug($ib["label"])}}' class='btn {{($ib['color'])?'btn-'.$ib['color']:'btn-primary'}} btn-sm' 
                      @if($ib['onClick']) onClick='return {{$ib["onClick"]}}' @endif
                      @if($ib['onMouseOever']) onMouseOever='return {{$ib["onMouseOever"]}}' @endif
                      @if($ib['onMoueseOut']) onMoueseOut='return {{$ib["onMoueseOut"]}}' @endif
                      @if($ib['onKeyDown']) onKeyDown='return {{$ib["onKeyDown"]}}' @endif
                      @if($ib['onLoad']) onLoad='return {{$ib["onLoad"]}}' @endif
                      >
                        <i class='{{$ib["icon"]}}'></i> {{$ib["label"]}}
                      </a>
                    @endforeach                                                          
            @endif
            <!-- END BUTTON -->
          </h1>


          <ol class="breadcrumb">
            <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
            <li class="active">@if($module->name == 'location')  Company Branches  @else  {{ $module->name }} @endif </li>
          </ol>
          @else
          <h1>{{Session::get('appname')}} <small>DASHBOARD</small></h1>
          @endif
        </section>  
    

        <!-- Main content -->
        <section id='content_section' class="content">

          @if(@$alerts)
            @foreach(@$alerts as $alert)
              <div class='callout callout-{{$alert[type]}}'>                
                  {!! $alert['message'] !!}
              </div>
            @endforeach
          @endif


    <!--  @if (Session::get('message')!='')
      <div class='alert alert-{{ Session::get("message_type") }}'>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-info"></i> {{ trans("crudbooster.alert_".Session::get("message_type")) }}</h4>
        {!!Session::get('message')!!}
      </div>
      @endif -->



            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('crudbooster::footer')

</div><!-- ./wrapper -->


@include('crudbooster::admin_template_plugins')

<!-- load js -->
@if($load_js)
  @foreach($load_js as $js)
    <script src="{{$js}}"></script>
  @endforeach
@endif
<script type="text/javascript">
  var site_url = "{{url('/')}}" ;
  @if($script_js)
    {!! $script_js !!}
  @endif 
</script>    

@stack('bottom')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</body>
</html>
