@extends('crudbooster::admin_template')
@section('content')
        
        <div >

        @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
          @if(g('return_url'))
          <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i> &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>       
          @else
          <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i> &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>       
          @endif
        @endif
        
        <div class="panel panel-default">
           <div class="panel-heading">
             <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i>
              @if(CRUDBooster::getCurrentMethod() == 'getAdd')
               Add
              @else
              Edit
              @endif
               <!-- {!! $page_title or "Page Title"  !!} -->
              </strong>
           </div> 
      @if($parent_field == 'parent_option_id')
        @php 
          $parent_ = DB::table('tbl_catoptions')->where('id',$parent_id)->where('parent_option_id','!=',0)->get();
      $parent2[] = $parent_[0];
         function recur($parent2) {
      foreach($parent2 as $key=>$pa) {
          $par_parent = DB::table('tbl_catoptions')->where('id',$pa->parent_option_id)->first();
        $second[] = $par_parent;
        if($par_parent->parent_option_id) {
          $parent2[$key] = $par_parent;
          return array_merge($second,recur($parent2));
        }else{
        return $second;
        }
       }
    }
      $second = recur($parent2);
        sort($second);
      $first[] = $parent_[0];
      if($second[0])
        $parent_table1 = array_merge($second,$first);
      else
        $parent_table1 = $first;
       $company_name = DB::table('tbl_service')->where('id',$parent_table1[0]->company_id )->pluck('service_name')->first();
     @endphp
    {{--    <div style="text-align: center;font-size:15px;">   
         <strong s>{{ $company_name }}</strong>
         <hr style="margin:0px;">
        @foreach($parent_table1 as $parent_table)
       @if ($loop->last)
        <span ><a href="/admin/ivr_details?parent_table=tbl_catoptions&parent_columns=option_name,option_value,company_id&parent_columns_alias=&parent_id={{ $parent_table->id}}&foreign_key=parent_option_id&label=SubIVR&fore=company_id&return_url=urlencode(Request::fullUrl())">
          {{ $parent_table->option_name }} ({{ $parent_table->option_value }})
        </a></span>
        @else
          <span ><a href="/admin/ivr_details?parent_table=tbl_catoptions&parent_columns=option_name,option_value,company_id&parent_columns_alias=&parent_id={{ $parent_table->id}}&foreign_key=parent_option_id&label=SubIVR&fore=company_id&return_url=urlencode(Request::fullUrl())">
            {{ $parent_table->option_name }} ({{ $parent_table->option_value }}) 
          </a><i class="fa fa-arrow-right" style="font-size:12px;"></i></span>
        @endif
    @endforeach
    </div> --}}
       
      @endif
           <div class="panel-body" style="padding:20px 0px 0px 0px">
                <?php                               
                  $action = (@$row)?CRUDBooster::mainpath("edit-save/$row->id"):CRUDBooster::mainpath("add-save");
                  $return_url = ($return_url)?:g('return_url');          
                ?>
                <form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action='{{$action}}'>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">    
                <input type='hidden' name='return_url' value='{{ @$return_url }}'/>                      
                <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>      
                <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                @if($hide_form)
                  <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                @endif
                        <div class="box-body" id="parent-form-area" style="padding:0px 100px !important;">

                          @if($command == 'detail')
                             @include("crudbooster::default.form_detail")  
                          @else
                             @include("crudbooster::default.form_body")         
                          @endif
                        </div><!-- /.box-body -->
                
                        <div class="box-footer" style="background: #F5F5F5">  
                          
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')                       
                                <!-- @if(g('return_url'))
                                <a href='{{g("return_url")}}' class='btn btn-default'><i class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                @else 
                                <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                @endif -->
                                 @if(Request::segment(3) != 'edit')
                                <a id="reset" class="btn btn-default">Reset</a> 
                                 @endif

                              @endif
                              @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                 @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')                                                                                                     
                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                 @endif

                                 @if($button_save && $command != 'detail')
                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success'>
                                 @endif
                              @if(Request::segment(2) == 'users' && Request::segment(2) != 'detail')
                                <a onclick="return confirm('Are you sure want to delete?')" href="{{url('user/delete/'.$row->id)}}" id="reset" class="btn btn-danger btn-delete">Delete</a>
                              @endif
                                 
                              @endif
                            </div>
                          </div>                             
                              
                          

                        </div><!-- /.box-footer-->

                        </form>
        
            </div>
        </div>
        </div><!--END AUTO MARGIN-->

@endsection
