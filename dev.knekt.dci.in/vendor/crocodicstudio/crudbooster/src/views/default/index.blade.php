@extends('crudbooster::admin_template')
@php
$user = CRUDBooster::me();

if($user) 
  app()->setlocale($user->language);
@endphp
@section('content')

   @if($index_statistic)
      <div id='box-statistic' class='row'>
      @foreach($index_statistic as $stat)
          <div  class="{{ ($stat['width'])?:'col-sm-3' }}">
              <div class="small-box bg-{{ $stat['color']?:'red' }}">
                <div class="inner">
                  <h3>{{ $stat['count'] }}</h3>
                    <p>{{ $stat['label'] }}</p>
                </div>
                <div class="icon">
                    <i class="{{ $stat['icon'] }}"></i>
                </div>                    
              </div>
          </div>
      @endforeach
      </div>
    @endif

   @if(!is_null($pre_index_html) && !empty($pre_index_html))
       {!! $pre_index_html !!}
   @endif





    <!-- get company details-->
 <?php
     $company_name_id = DB::table('tbl_service')->where('status',1)
                          ->select('service_name','id','phone')->get();
     $company_count   = $company_name_id->count();
  ?>
   <!-- get company details-->





    @if(g('return_url'))
   <p><a href='{{g("return_url")}}'><i class='fa fa-chevron-circle-{{ trans('crudbooster.left') }}'></i> &nbsp; {{trans('crudbooster.form_back_to_list',['module'=>urldecode(g('label'))])}}</a></p>
    @endif

    @if($parent_table)
    <div class="box box-default">
      <div class="box-body table-responsive no-padding">
        @php 

    

      $parent_ = DB::table('tbl_catoptions')->where('id',$parent_table->id)->where('parent_option_id','!=',0)->pluck('parent_option_id');
      $parent2[] = $parent_[0];
    function recur($parent2) {
      foreach($parent2 as $key=>$pa) {
          $par_parent = DB::table('tbl_catoptions')->where('id',$pa)->first();
        $second[] = $par_parent;
        if($par_parent->parent_option_id) {
          $parent2[$key] = $par_parent->parent_option_id;
          return array_merge($second,recur($parent2));
        }else{
        return $second;
        }
       }
    }
      $second = recur($parent2);
        sort($second);

      $first[] = $parent_table;
      if($second[0])
        $parent_table1 = array_merge($second,$first);
      else
        $parent_table1 = $first;
       $company_name = DB::table('tbl_service')->where('id',$parent_table1[0]->company_id )->pluck('service_name')->first();
        @endphp
        @if(count($parent_table1) == 0)
        @foreach($parent_table1 as $parent_table)
        <table class='table table-bordered' style="width:auto;float:left">
          <tbody>
            <tr class='active'>
              <td colspan="2"><strong><i class='fa fa-bars'></i> {{ ucwords(urldecode(g('label'))) }}</strong></td>
            </tr>
            @foreach(explode(',',urldecode(g('parent_columns'))) as $c)
            <tr>
              <td width="auto"><strong>
                @if($c == 'option_name')
                     IVR Name 
                @elseif($c == 'option_value')
                   IVR Value 
                @else 
                   Company Name
                @endif
              <!--  @if(urldecode(g('parent_columns_alias')))
              {{explode(',',urldecode(g('parent_columns_alias')))[$loop->index]}}
              @else
              {{  ucwords(str_replace('_',' ',$c)) }}
               @endif -->
              </strong></td><td>
                @php 
                 if($c == 'company_id')
                    $company_name = DB::table('tbl_service')->where('id',$parent_table->$c )->pluck('service_name')->first();
                @endphp 
                @if($c == 'company_id')
                    {{ $company_name }}
                @else
                   {{ $parent_table->$c }}
                @endif
              </td>
            </tr>
            @endforeach            
          </tbody>
        </table> 
        @endforeach 
        @else  
         <div style="text-align: center;font-size:15px;">   
         <strong>{{ $company_name }}</strong>
         <hr style="margin:0px;">
        @foreach($parent_table1 as $keys=>$parent_table)
        <?php  $parent_phone = DB::table('tbl_catoptions')->where('id',$parent_table->id)->select('phone')->first(); ?>
       @if ($loop->last)
        <span ><a href="/admin/ivr_details?parent_table=tbl_catoptions&parent_columns=option_name,option_value,company_id&parent_columns_alias=&parent_id={{ $parent_table->id}}&foreign_key=parent_option_id&label=SubIVR&fore=company_id,phone&return_url={{urlencode(Request::fullUrl())}}">

          {{ $parent_table->option_name }} ({{ $parent_table->option_value }})  @if($keys == 0)
           ({{ $parent_phone->phone }}) 
           @endif  
        </a>
        </span>
        @else
          <span ><a href="/admin/ivr_details?parent_table=tbl_catoptions&parent_columns=option_name,option_value,company_id&parent_columns_alias=&parent_id={{ $parent_table->id}}&foreign_key=parent_option_id&label=SubIVR&fore=company_id,phone&return_url={{urlencode(Request::fullUrl())}}">{{ $parent_table->option_name }} ({{ $parent_table->option_value }}) 
           @if($keys == 0)
           ({{ $parent_phone->phone }}) 
           @endif  
          </a>
            <i class="fa fa-arrow-right"  style="font-size:12px;"></i></span>
        @endif
    @endforeach
    </div>
        @endif
      </div>
    </div>
    @endif
 
    <div class="box">
      <div class="box-header">  
        @if($button_bulk_action && ( ($button_delete && CRUDBooster::isDelete()) || $button_selected) )
        <div class="pull-{{ trans('crudbooster.left') }}">          
          <div class="selected-action" style="display:inline-block;position:relative;">
              
  <!-- rbr hide table -->
      @if(Request::segment(2) !='ivr_details') 

              <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><!-- <i class='fa fa-check-square-o'></i> --> {{trans("crudbooster.button_selected_action")}}
                <span class="fa fa-caret-down"></span></button>                              
              <ul class="dropdown-menu">      
    
                @if($button_delete && CRUDBooster::isDelete())                                                              
                <li><a href="javascript:void(0)" data-name='delete' title='{{trans('crudbooster.action_delete_selected')}}'><i class="fa fa-trash"></i> {{trans('crudbooster.action_delete_selected')}}</a></li>
                @endif 
  
      @endif    
  <!-- rbr Endif -->        

                @if($button_selected)
                  @foreach($button_selected as $button)
                    <li><a href="javascript:void(0)" data-name='{{$button["name"]}}' title='{{$button["label"]}}'><i class="fa fa-{{$button['icon']}}"></i> {{$button['label']}}</a></li>
                  @endforeach
                @endif

              </ul><!--end-dropdown-menu-->
          </div><!--end-selected-action-->        
        </div><!--end-pull-left-->
        @endif
        <div class="box-tools pull-{{ trans('crudbooster.right') }}" style="position: relative;margin-top: -5px;margin-right: -10px">
          
     <!-- rbr hide table -->
      @if(Request::segment(2) !='ivr_details')    
     <!-- rbr -->
       
              @if($button_filter)
              <a style="margin-top:-23px" href="javascript:void(0)" id='btn_advanced_filter' data-url-parameter='{{$build_query}}' title='{{trans('crudbooster.filter_dialog_title')}}' class="btn btn-sm btn-default {{(Request::get('filter_column'))?'active':''}}">                               
                <i class="fa fa-filter"></i> {{trans("crudbooster.button_filter")}}
              </a>
              @endif


            <form method='get' style="display:inline-block;width: 260px;" action='{{Request::url()}}'>
                <div class="input-group">
                  <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control input-sm pull-{{ trans('crudbooster.right') }}" placeholder="{{trans('crudbooster.filter_search')}}"/>
                  {!! CRUDBooster::getUrlParameters(['q']) !!}
                  <div class="input-group-btn">
                    @if(Request::get('q'))
                    <?php 
                      $parameters = Request::all();
                      unset($parameters['q']);
                      $build_query = urldecode(http_build_query($parameters));
                      $build_query = ($build_query)?"?".$build_query:"";
                      $build_query = (Request::all())?$build_query:"";
                    ?>
                    <button type='button' onclick='location.href="{{ CRUDBooster::mainpath().$build_query}}"' title="{{trans('crudbooster.button_reset')}}" class='btn btn-sm btn-warning'><i class='fa fa-ban'></i></button>
                    @endif
                    <button type='submit' class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
            </form>

     
          <form method='get' id='form-limit-paging' style="display:inline-block" action='{{Request::url()}}'>  {!! CRUDBooster::getUrlParameters(['limit']) !!}
              <div class="input-group">
                <select onchange="$('#form-limit-paging').submit()" name='limit' style="width: 56px;"  class='form-control input-sm'>
                    <option {{($limit==5)?'selected':''}} value='5'>5</option> 
                    <option {{($limit==10)?'selected':''}} value='10'>10</option>
                    <option {{($limit==20)?'selected':''}} value='20'>20</option>
                    <option {{($limit==25)?'selected':''}} value='25'>25</option>
                    <option {{($limit==50)?'selected':''}} value='50'>50</option>
                    <option {{($limit==100)?'selected':''}} value='100'>100</option>
                    <option {{($limit==200)?'selected':''}} value='200'>200</option>
                </select>                              
              </div>
            </form>
      @endif

        </div> 

        <br style="clear:both"/>

      </div>
      <div class="box-body table-responsive no-padding">
        @if(Request::segment(2) !='ivr_details')
            @include("crudbooster::default.table")
        @else



 <!-- -->

              <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                <div class="scoop-inner-navbar">                
                 
                
                    <input type="hidden" id="language"     name="language"    value=''>
                    <input type="hidden" id="company_id"   name="company_id"  value=''>
                    <input type="hidden" id="subcategory"  name="subcategory" value=''>
                    <input type="hidden" id="subcategory_name"  name="subcategory_name" value=''>
                    <input type="hidden" id="category_tab"     name="category_tab" value=''>
                    <input type="hidden" id="subcategory_tab"  name="subcategory_tab" value=''>
                    <input type="hidden" id="company_phone"  name="company_phone" value=''>
                    <input type="hidden" id="current_tab"  name="current_tab" value=''>
                    <input type="hidden" id="form_post"  name="form_post" value='0'>
 
<div class="row" style="min-height:600px;">
       <div  class="col-sm-12">       
           <div class="flow_gird clearfix">


               <div id="tab1" class="tabb-1">
                  <ul class="nav nav-tabs tabs-left">                    
                    @if($company_name_id)
                      @foreach($company_name_id as $key=>$company)                                
    <li class=""><a class="companyid_{{$company->id}}_{{$company->phone}}" >{{$company->service_name}}</a>
    <i class="fa fa-clone clone_popup clone_{{$company->id}}_{{$company->phone}}" aria-hidden="true" data-toggle="modal" href=".modal_nnn"></i> </li>
   
                      @endforeach
                    @endif
               
                  </ul>
               </div>


              <div id="tab2" class="tabb-2 tabbs" style="display: none;">               
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus " id="form_popup" data-toggle="modal" data-target="#category_form"></span>

                  </ul>
              </div>
       
              <div id="tab3" class="tabb-3 tabbs" style="display: none;">                  
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div>

              <div id="tab4" class="tabb-4 tabbs" style="display: none;">                 
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div>

              <div id="tab5" class="tabb-5 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul> 
              </div>

              <div id="tab6" class="tabb-6 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div>

              <div id="tab7" class="tabb-7 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                    <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div>

              <div id="tab8" class="tabb-8 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div>

              <div id="tab9" class="tabb-9 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div> 

              <div id="tab10" class="tabb-10 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div> 

<div id="tab11" class="tabb-11 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div> 

<div id="tab12" class="tabb-12 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div> 

<div id="tab13" class="tabb-13 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div> 

<div id="tab14" class="tabb-14 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div> 

<div id="tab15" class="tabb-15 tabbs" style="display: none;">
                  <ul class="nav nav-tabs tabs-left">
                     <span class="glyphicon glyphicon-plus form_popup_subcate" data-toggle="modal" href=".modal_nn"></span>
                  </ul>
              </div> 



            </div>  
        </div>
  </div>




<!-- show popup form -->
  <span class="glyphicon glyphicon-plus" id="subcat_popup_form" 
   data-toggle="modal" href=".modal_nn" style="display: none;"></span>





<div id="category_form" class="modal fade" role="dialog" data-target="#form_popup"  tabindex="-1" data-focus-on="input:first">
  <div class="modal-dialog" >
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
  </div>
     <div class="sucess_msg"> </div>
       <form class='form-horizontal' name="cate_form" id="cate_form" enctype="multipart/form-data">

       <div class="left">IVR LANGUAGE *  :</div>
       <div class="right"><select id="option_name" name="option_name">
                                <option value="English"> English </option>
                                <option value="Arabic">  Arabic  </option> 
                          </select>
                          <span class="option_name_errMsg"></span>
        </div>
        <div class="left">IVR Value *     :</div>
         <div class="right"><input type="text" name="option_value" id="option_value" value="">                         <span class="option_value_errMsg"></span>
         </div>          
         <div class="left">Time Delay      :</div>
         <div class="right"><input type="time" id="time_delay" name="time_delay" min='00:00' max= '10:00' value='00:00'>
            <span class="time_delay_errMsg"></span>                         
         </div>
         <div class="left">                        
                 
         </div>   
         <div class="modal-footer">
		  <input type="submit" id="add_cate_form" value="Submit" class="btn btn-primary">
            <button type="button" data-dismiss="modal" class="btn sub_close_popup">Close</button>
             <!-- <button type="button" class="btn btn-primary">Ok</button> -->
         </div>             
            
        </form>
     </div>
  </div>



 
  <div id="sub_category_form" class="modal modal_nn fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">
     <div class="modal-header">
       <button type="button" class="close sub_close_popup" data-dismiss="modal" aria-hidden="true">X</button>
     </div>
     <div class="sub_sucess_msg"> </div>
       <form class='form-horizontal' name="sub_cate_form" id="sub_cate_form" enctype="multipart/form-data">

       <div class="left">Service Name *  </div>
          <div class="right">
              <input type="text" name="sub_option_name" id="sub_option_name" value="">
              <span class="sub_option_name_errMsg"></span>       
          </div>
        <div class="left">Ivr Value *     </div>
          <div class="right"><input type="text" name="sub_option_value" id="sub_option_value" value="">                         <span class="sub_option_value_errMsg"></span>
          </div>         
         
         <div class="left">Time Delay      </div>
            <div class="right"><input type="time" id="sub_time_delay" name="sub_time_delay"  min='00:00' max= '10:00' value='00:00'>
                          <span class="sub_time_delay_errMsg"></span>                         
         </div>       

  <div class="modal-footer">
     <button type="button" id="close_popup" data-dismiss="modal" class="btn sub_close_popup">Close</button>
     <input type="submit"  id="add_sub_form"    value="Submit" class="btn">
  </div>               
            
        </form>
     </div>
  </div>



<div id="clone_form" class="modal modal_nnn fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">
     <div class="modal-header">
       <button type="button" class="close clone_close" data-dismiss="modal" aria-hidden="true">X</button>
     </div>
     <div class="clone_sucess_msg"> </div>
       <form class='form-horizontal' name="clone_forms" id="clone_forms" enctype="multipart/form-data">
           <div class="left">Service Name *  </div>
              <div class="right">
                  <input type="text" name="clone_service_name" id="clone_service_name" value="">
                  <span class="clone_service_name_errMsg"></span>       
              </div>  
              <div class="left">Company Name *  </div>
              <div class="right">                 
                  <select id="clone_company_name" name="clone_company_name">
                   @if($company_name_id)
                               <option value=""> Please select company name </option>
                      @foreach($company_name_id as $key=>$company) 
                                <option value="{{$company->id}}"> {{ $company->service_name }} </option>
                      @endforeach
                   @endif
                               
                  </select>
                  <span class="clone_comapny_name_errMsg"></span>       
              </div>  
             <div class="modal-footer">
               <button type="button"   data-dismiss="modal" class="btn clone_close">Close</button>
               <input type="submit"   id="clone_button"    value="Clone" class="btn btn-primary">
             </div>             
        </form>
     </div>
  </div>



                <!-- <div class="scoop-navigatio-lavel">labels</div> -->
              </div> 
            


        @endif

      </div>
    </div>

   @if(!is_null($post_index_html) && !empty($post_index_html))
       {!! $post_index_html !!}
   @endif    

@endsection
