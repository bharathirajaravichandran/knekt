    @push('bottom')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">    
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
@php
    $active = DB::table('cms_users')->where('status',1)->where('id_cms_privileges','!=',1)->count();

    $app_user_count = DB::table('cms_users')->where('status',1)->where('user_type','app')->count();
    
    $language_arabic = DB::table('tbl_language_time')->where('language_name','arabic')->first();
    $language_english = DB::table('tbl_language_time')->where('language_name','english')->first();

    $company_usage = DB::table('tbl_company_time')->get();
    

    $inactive = DB::table('cms_users')->where('status',0)->where('id_cms_privileges','!=',1)->count();
    $active_com = DB::table('tbl_service')->where('status',1)->count();
    $inactive_com = DB::table('tbl_service')->where('status',0)->count();
    $data_label =  $data_value = [];
    //Bar char info
    $data = DB::table('tbl_reports')
                    ->join('tbl_service','tbl_reports.service_id','=','tbl_service.id')
                    ->select(DB::raw('count(*) as count, tbl_service.service_name'))
                    ->groupBy('tbl_reports.service_id')
                     ->get();
        foreach($data as $da) {
            $data_label[] = (string)$da->service_name;
            $data_value[] = $da->count;
            }

    $data_labe = implode(',',$data_label);
    $data_va = implode(',',$data_value);
@endphp
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ajaxStart(function() {
                $('.btn-save-statistic').html("<i class='fa fa-spin fa-spinner'></i>");
            })
            $(document).ajaxStop(function() {
                $('.btn-save-statistic').html("<i class='fa fa-save'></i> Auto Save Ready");
            })

            $('.btn-show-sidebar').click(function(e)  {
                e.stopPropagation();
            })
            $('html,body').click(function() {
                $('.control-sidebar').removeClass('control-sidebar-open');
            })
        })
    </script>
        <script>
var ctx = document.getElementById("myChart");
var ctx1 = document.getElementById("myChart1");
var ctx2 = document.getElementById("myChart2");
var ctx3 = document.getElementById("myChart3");
var ctx4 = document.getElementById("myChart4");
var ctx5 = document.getElementById("myChart5");
var ctx6 = document.getElementById("myChart6");

var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ["Active Users", "InActive Users"],
        datasets: [{
            label: 'Users',
            data: [{{ $active }}, {{ $inactive }}],
            backgroundColor: [
                '#ff6384',
                '#36a2eb'
                
            ],
            borderColor: [
                '#ff6384',
                '#36a2eb',
                
            ],
           
        }]
    },
   
});

var myChart1 = new Chart(ctx1, {
    type: 'pie',
    data: {
        labels: ["Active Companies", "InActive Companies"],
        datasets: [{
            label: 'Companies',
            data: [{{ $active_com }}, {{ $inactive_com }}],
            backgroundColor: [
                '#cc65fe',
                '#ffce56'
                
            ],
            borderColor: [
                '#cc65fe',
                '#ffce56',
                
            ],
           
        }]
    },
   
});


var myChart3 = new Chart(ctx3, {
    type: 'polarArea',
    data: {
        labels: ["Airtel", "Lg","Sony","Honda"],
        datasets: [{
            label: 'Companies',
            data: [30, 10,20,8],
            backgroundColor: [
                '#ffce56',
                '#00A65E',
                '#d9f442',
                '#f441ca'
                
            ],
            borderColor: [
                '#ffce56',
                '#00A65E',
                '#d9f442',
                '#f441ca'
                
            ],
           
        }]
    },
   
});










var myChart4 = new Chart(ctx4, {   
    type: 'horizontalBar',
    data: {
        labels: ["kuwait", "India" , "Oman" ],
        datasets: [{
            label: 'Users',
            data: [ 25,5,13],
            backgroundColor: [
                '#8c7cc1',
                '#cc4b4f'
                
            ],
            borderColor: [
                '#8c7cc1',
                '#cc4b4f',
                
            ],
           
        }]
    },
   
});

var myChart5 = new Chart(ctx5, {   
    type: 'doughnut',
    data: {
        labels: ["{{ $language_arabic->language_name }}" , "{{ $language_english->language_name }} "],
        datasets: [{
            label: 'Votes',
            data: [ {{ $language_arabic->time }},{{ $language_english->time }}],
            backgroundColor: [
                '#0845a8',
                '#08a82d'
                
            ],
            borderColor: [
                '#0845a8',
                '#08a82d',
                
            ],
           
        }]
    },
   
});

var myChart6 = new Chart(ctx6, {   
    type: 'radar',
    data: {
        labels: ["{{$company_usage[0]->company_name}}", "{{$company_usage[1]->company_name}}" , "{{ $company_usage[2]->company_name }}","{{ $company_usage[3]->company_name }}","Airtel" ],
        datasets: [{
            label: 'Company',
            data: [ {{$company_usage[0]->call_time}} ,{{$company_usage[1]->call_time}},{{$company_usage[2]->call_time}},{{$company_usage[3]->call_time}},"50"],
            backgroundColor: [                
                '#e0a15e'                
                
            ],
            borderColor: [               
                '#e0a15e'               
                
            ],
           
        }]
    },
   
});


var myChart2 = new Chart(ctx2, {
    type: 'bar',
    axisYType: "secondary",
    data: {
        labels: [
           "App Users",           
           'Application Used times'
                ],
        datasets: [{
            label: 'Services Used',
            data: [{{ $app_user_count }},30],
            backgroundColor: [
                '#cc65fe',                
                '#3336FF'
                
            ],
            borderColor: [
                '#cc65fe',                
                '#3336FF'
                
            ],
           
        }]
    },
   
});



</script>
    @endpush
    @push('head')
    <style type="text/css">
        .control-sidebar ul {
            padding:0 0 0 0;
            margin:0 0 0 0;            
            list-style-type:none;
        }
        .control-sidebar ul li {
            text-align: center;
            padding: 10px;            
            border-bottom: 1px solid #555555;
        }
        .control-sidebar ul li:hover {
            background: #555555;
        }
        .control-sidebar ul li .title {
            text-align: center;
            color: #ffffff;            
        }
        .control-sidebar ul li img {
            width: 100%;            
        }

        ::-webkit-scrollbar {
            width: 5px;
            height: 5px;
        }
    </style>
    @endpush

    @push('bottom')
    <!-- ADDITION FUNCTION FOR BUTTON -->
    <script type="text/javascript">
        var id_cms_statistics = '{{$id_cms_statistics}}';

        function addWidget(id_cms_statistics,area,component) {      
            var id = new Date().getTime();
            $('#'+area).append("<div id='"+id+"' class='area-loading'><i class='fa fa-spin fa-spinner'></i></div>");

            var sorting = $('#'+area+' .border-box').length;             
            $.post("{{CRUDBooster::mainpath('add-component')}}",{component_name:component,id_cms_statistics:id_cms_statistics,sorting:sorting,area:area},function(response) {
                $('#'+area).append(response.layout);   
                $('#'+id).remove();                
            })
        }

    </script>
    <!--DATATABLE-->    
    <link rel="stylesheet" href="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/datatables/dataTables.bootstrap.css')}}">
    <script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!--END HERE-->
    @endpush


    @push('head')
    <!-- jQuery UI 1.11.4 -->
    <style type="text/css">
        .sort-highlight {
            border:3px dashed #cccccc;                    
        }
        .layout-grid {
            border:1px dashed #cccccc;
            min-height: 150px;
        }
        .layout-grid + .layout-grid {
            border-left:1px dashed transparent;            
        }
        .border-box {           
            position: relative;         
        }
        .border-box .action {           
            font-size: 20px;
            display: none;
            text-align: center;
            display: none;
            padding:3px 5px 3px 5px;
            background:#DD4B39;
            color:#ffffff;
            width: 70px;
            -webkit-border-bottom-right-radius: 5px;
            -webkit-border-bottom-left-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -moz-border-radius-bottomleft: 5px;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
            position: absolute;
            margin-top: -20px;          
            right: 0;
            z-index: 999;
            opacity: 0.8;   
        }
        .border-box .action a {
            color: #ffffff;
        }
        
        .border-box:hover {
            /*border:2px dotted #BC3F30;*/
        }
        
        @if(CRUDBooster::getCurrentMethod() == 'getBuilder')
        .border-box:hover .action {
            display: block;
        }
        .panel-heading, .inner-box, .box-header, .btn-add-widget {
            cursor: move;
        }
        @endif
        
        .connectedSortable {
            position: relative;
        }
        .area-loading {        
            position: relative; 
            width: 100%;  
            height: 130px;          
            background: #dedede;
            border: 4px dashed #cccccc;
            font-size: 50px;
            color: #aaaaaa;
            margin-bottom: 20px;
        }
        .area-loading i {           
            position: absolute;
            left:45%;
            top:30%;            
            transform: translate(-50%, -50%);                 
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @endpush

    @push('bottom')
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>    
    <script type="text/javascript">
    $(function() {                     

        var cloneSidebar = $('.control-sidebar').clone();

        @if(CRUDBooster::getCurrentMethod() == 'getBuilder')
            createSortable();        
        @endif

        function createSortable() {
            $(".connectedSortable").sortable({
                placeholder: "sort-highlight",
                connectWith: ".connectedSortable",
                handle: ".panel-heading, .inner-box, .box-header, .btn-add-widget",            
                forcePlaceholderSize: true,
                zIndex: 999999,
                stop: function(event, ui) {
                    console.log(ui.item.attr('class'));
                    var className = ui.item.attr('class');
                    var idName = ui.item.attr('id');
                    if(className == 'button-widget-area') {
                        var areaname = $('#'+idName).parent('.connectedSortable').attr('id');
                        var component = $('#'+idName+' > a').data('component');
                        console.log(areaname);
                        $('#'+idName).remove();
                        addWidget(id_cms_statistics,areaname,component);                        
                        $('.control-sidebar').html(cloneSidebar);
                        cloneSidebar = $('.control-sidebar').clone(); 
                         
                        createSortable();             
                    }
                },
                update: function(event, ui){
                    if(ui.sender){
                        var componentID = ui.item.attr('id');
                        var areaname = $('#'+componentID).parent('.connectedSortable').attr("id");
                        var index = $('#'+componentID).index();

                        
                        $.post("{{CRUDBooster::mainpath('update-area-component')}}",{componentid:componentID,sorting:index,areaname:areaname},function(response) {
                            
                        })
                    }
                }
              });
        }
           
    })
     
    </script>

    <script type="text/javascript">
        $(function() {
            
            $('.connectedSortable').each(function() {
                var areaname = $(this).attr('id');
                
                $.get("{{CRUDBooster::mainpath('list-component')}}/"+id_cms_statistics+"/"+areaname,function(response) {            
                    if(response.components) {
                        
                        $.each(response.components,function(i,obj) {
                            $('#'+areaname).append("<div id='area-loading-"+obj.componentID+"' class='area-loading'><i class='fa fa-spin fa-spinner'></i></div>");
                            $.get("{{CRUDBooster::mainpath('view-component')}}/"+obj.componentID,function(view) {
                                console.log('View For CID '+view.componentID);
                                $('#area-loading-'+obj.componentID).remove();
                                $('#'+areaname).append(view.layout);
                                
                            })
                        })                      
                    }                   
                })
            })
                   
            
            $(document).on('click','.btn-delete-component',function() {
                var componentID = $(this).data('componentid');
                var $this = $(this);

                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this widget !",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes",
                  closeOnConfirm: true
                },
                function(){
                    
                    $.get("{{CRUDBooster::mainpath('delete-component')}}/"+componentID,function() {
                        $this.parents('.border-box').remove();
                        
                    });
                });
                
            })
            $(document).on('click','.btn-edit-component',function() {
                var componentID = $(this).data('componentid');
                var name        = $(this).data('name');

                $('#modal-statistic .modal-title').text(name);
                $('#modal-statistic .modal-body').html("<i class='fa fa-spin fa-spinner'></i> Please wait loading...");
                $('#modal-statistic').modal('show');

                $.get("{{CRUDBooster::mainpath('edit-component')}}/"+componentID,function(response) {
                    $('#modal-statistic .modal-body').html(response);
                })
            })

            $('#modal-statistic .btn-submit').click(function() {         
                
                $('#modal-statistic form .has-error').removeClass('has-error');

                var required_input = [];
                $('#modal-statistic form').find('input[required],textarea[required],select[required]').each(function() {
                    var $input = $(this);
                    var $form_group = $input.parent('.form-group');
                    var value = $input.val();

                    if(value == '') {
                        required_input.push($input.attr('name'));
                    }
                })    

                if(required_input.length) {  
                    setTimeout(function() {
                        $.each(required_input,function(i,name) {
                            $('#modal-statistic form').find('input[name="'+name+'"],textarea[name="'+name+'"],select[name="'+name+'"]').parent('.form-group').addClass('has-error');
                        })
                    },200);                  
                    
                    return false;
                }

                var $button = $(this).text('Saving...').addClass('disabled');
                
                $.ajax({
                    data:$('#modal-statistic form').serialize(),
                    type:'POST',
                    url:"{{CRUDBooster::mainpath('save-component')}}",
                    success:function() {
                        
                        $button.removeClass('disabled').text('Save Changes');
                        $('#modal-statistic').modal('hide');
                        window.location.href = "{{Request::fullUrl()}}";
                    },
                    error:function() {
                        alert('Sorry something went wrong !');
                        $button.removeClass('disabled').text('Save Changes');
                    }
                })
            })
        })
    </script>
    @endpush

    <div id='modal-statistic' class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <p>One fine body&hellip;</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn-submit btn btn-primary" data-loading-text="Saving..." autocomplete="off">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div id='statistic-area'>


        <div class="statistic-row row">
            <div id='area1' class="col-sm-3 connectedSortable">             

            </div>
            <div id='area2' class="col-sm-3 connectedSortable">
               
            </div>
            <div id='area3' class="col-sm-3 connectedSortable">

            </div>
            <div id='area4' class="col-sm-3 connectedSortable">
                
            </div>            
        </div>


        <div class="row">
            <div class="col-md-6">
               <div class="box box-danger">
                         <div class="box-header with-border">
                              <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Users </span>
                             <!-- <h3 class="box-title">User Info</h3> -->
                                <div class="box-body">
                                        <canvas id="myChart" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                
            </div>

            <div class="col-md-6">
               <div class="box box-danger">

                         <div class="box-header with-border">   
                          <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Companies </span>                        
                                <div class="box-body">
                                        <canvas id="myChart1" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                 
            </div>




            <div class="col-md-6">
               <div class="box box-danger">
                         <div class="box-header with-border">
                             <!-- <h3 class="box-title">User <I></I>nfo</h3> -->
                                <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;">Number of Times a Company is Called </span>
                                <div class="box-body">
                                        <canvas id="myChart3" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                 
            </div>

            <div class="col-md-6">
               <div class="box box-danger">
                         <div class="box-header with-border">                         
                               <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Demographics of Users</span>
                                <div class="box-body">
                                        <canvas id="myChart4" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                
            </div>


            <div class="col-md-6">
               <div class="box box-danger">
                         <div class="box-header with-border">
                             <!-- <h3 class="box-title">User <I></I>nfo</h3> -->
                                <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;">Statistics on language selected </span>
                                <div class="box-body">
                                        <canvas id="myChart5" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                 
            </div>

            <div class="col-md-6">
               <div class="box box-danger">
                         <div class="box-header with-border">                         
                               <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Time taken for the operator to answer the call by company</span>
                                <div class="box-body">
                                        <canvas id="myChart6" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                
            </div>





             <div class="col-md-5">
                <div class="box box-danger">
                         <div class="box-header with-border">
                                 <div class="box-body">
                                        <canvas id="myChart2" width="150" height="100"></canvas>
                                 </div> 
                         </div>
                </div>  

             </div>
           
        </div>


    
        



    </div><!--END STATISTIC AREA-->
